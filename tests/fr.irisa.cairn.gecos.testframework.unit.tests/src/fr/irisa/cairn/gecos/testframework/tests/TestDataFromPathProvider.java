package fr.irisa.cairn.gecos.testframework.tests;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.testframework.data.AbstractCxxData;
import fr.irisa.cairn.gecos.testframework.data.CxxFileData;
import fr.irisa.cairn.gecos.testframework.dataprovider.DataFromPathProvider;
import fr.irisa.cairn.gecos.testframework.dataprovider.ResourcesLocation;
import fr.irisa.cairn.gecos.testframework.utils.DataProviderUtils;
import fr.irisa.cairn.gecos.testframework.utils.ResourceLocator;

/**
 * Test {@link DataFromPathProvider}.
 * 
 * @author aelmouss
 */
@RunWith(DataProviderRunner.class)
public class TestDataFromPathProvider {
	
	private static final boolean VERBOSE = false;

	//TODO: use a local dummy resources directory as well
	
	// Location 0
	private static final String BUN0 = "fr.irisa.cairn.gecos.testframework.resources";
	private static final String DIR0 = "src-c/single/gcc/compile/pass/fast";
	private static final ResourceLocator LOC0 = new ResourceLocator(BUN0, DIR0);
	private static final Path ROOT0 = LOC0.locate(DIR0);
	private static final List<Path> RES0 = findData(ROOT0, Arrays.asList(CxxFileData.class));

	// Location 1
	private static final String BUN1 = "fr.irisa.cairn.gecos.testframework.resources";
	private static final String DIR1 = "src-c/single/gcc/compile/pass/fast";
	private static final ResourceLocator LOC1 = new ResourceLocator(BUN1, DIR1);
	private static final Path ROOT1 = LOC1.locate(DIR1);
	private static final List<Path> RES1 = findData(ROOT1, Arrays.asList(CxxFileData.class));


	// Current resources
	private Stream<Path> res;

	
	public void runTest(AbstractCxxData data) {
		if(VERBOSE) System.out.println("   data: " + data.getSourceFiles());
		
		Assert.assertTrue(data != null);
		Path path = data.getPath();
		Assert.assertTrue(path != null);
		Assert.assertTrue(res.filter(path::equals).findAny().isPresent());
	}
	
	
	@Test
	@UseDataProvider(location = DataFromPathProvider.class, value = DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation(bundleName = BUN0, value = DIR0, dataClasses = {CxxFileData.class})
	public void testDataFrom1BundleLocationNoFilter(AbstractCxxData data) {
		this.res = RES0.stream();
		runTest(data);
	}

	@Test
	@UseDataProvider(location = DataFromPathProvider.class, value = DataFromPathProvider.PROVIDER_NAME)
	@ResourcesLocation(bundleName = BUN0, value = DIR0, dataClasses = {CxxFileData.class})
	@ResourcesLocation(bundleName = BUN1, value = DIR1, dataClasses = {CxxFileData.class})
	public void testDataFrom2BundleLocationNoFilter(AbstractCxxData data) {
		this.res = Stream.concat(RES0.stream(), RES1.stream());
		runTest(data);
	}
	
	private static List<Path> findData(Path dir, List<Class<? extends AbstractCxxData>> dataTypes) {
		try {
			return DataProviderUtils.findCompatibleResourcePaths(dir, Integer.MAX_VALUE, dataTypes);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
