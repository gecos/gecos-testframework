package fr.irisa.cairn.gecos.testframework.tests;

import static java.lang.Math.min;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.model.FrameworkMethod;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.testframework.dataprovider.AbstractDataProvider;
import fr.irisa.cairn.gecos.testframework.dataprovider.AddIntParams;
import fr.irisa.cairn.gecos.testframework.dataprovider.AddParams;
import fr.irisa.cairn.gecos.testframework.dataprovider.AddStringParams;
import fr.irisa.cairn.gecos.testframework.dataprovider.DataProviderFilter;
import fr.irisa.cairn.gecos.testframework.dataprovider.EmptyDataProvider;
import fr.irisa.cairn.gecos.testframework.dataprovider.Merge;
import fr.irisa.cairn.gecos.testframework.model.IData;
import fr.irisa.cairn.gecos.testframework.model.IDataFilter;

/**
 * Test {@link AbstractDataProvider}. 
 * 
 * @author aelmouss
 */
@RunWith(DataProviderRunner.class)
public class TestAbstractDataProvider {
	
	private static final boolean VERBOSE = false;

	private static final Map<Method, Integer> count = new HashMap<>();
	private static final Map<Method, Integer> expectedCount = new HashMap<>();
	
	
	private static final List<DummyData> DP1 = DummyDP1.data;
	private static final List<DummyData> F1DP1 = filterAll(DP1, F1.class);
	private static final List<DummyData> F2DP1 = filterAll(DP1, F2.class);

	private static final List<DummyData> DP2 = DummyDP2.data;
	private static final List<DummyData> F1DP2 = filterAll(DP2, F1.class);
	private static final List<DummyData> F2DP2 = filterAll(DP2, F2.class);
	
	private static final List<DummyData> F2F1DP1 = filterAll(F1DP1, F2.class);
	private static final List<DummyData> F2F1DP2 = filterAll(F1DP2, F2.class);

	private static final int N_DP1 = DP1.size();
	private static final int N_DP2 = DP2.size();
	private static final int N_F1DP1 = F1DP1.size();
	private static final int N_F1DP2 = F1DP2.size();
	private static final int N_F2DP1 = F2DP1.size();
	private static final int N_F2DP2 = F2DP2.size();
//	private static final int N_F2F1DP1 = F2F1DP1.size();
//	private static final int N_F2F1DP2 = F2F1DP2.size();

	
	public void initExpected(Method methodName, int expected) {
		if(!expectedCount.containsKey(methodName))
			expectedCount.put(methodName, expected);
	}
	
	public void runTest(Method methodName, DummyData data) {
		Integer c = count.get(methodName);
		c = c == null ? 1 : c+1;
		count.put(methodName, c);
	}
	
	@AfterClass
	public static void checkCount() {
		for (Method test : count.keySet()) {
			int c = count.get(test);
			int expected = expectedCount.get(test);
			if(VERBOSE) System.out.println("Total nb data for test " + test + " = " + c);
			
			Assert.assertEquals("test " + test.getName() + " count is not correct", expected, c);
		}
	}
	

/* ******************************************************
 *  Test @DataProviderFilter and @Merge
 * ******************************************************/
	
	@Test
	@UseDataProvider(location = DummyDP1.class, value = DummyDP1.PROVIDER_NAME)
	public void testUseNoF(DummyData d) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, N_DP1);
		
		Assert.assertTrue(DP1.contains(d));
		runTest(methodName, d);
	}

	@Test
	@UseDataProvider(location = DummyDP1.class, value = DummyDP1.PROVIDER_NAME)
	@DataProviderFilter(filter = F1.class)
	public void testUseF(DummyData d) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, N_F1DP1);
		
		Assert.assertTrue(F1DP1.contains(d));
		runTest(methodName, d);
	}
	
	@Test
	@UseDataProvider(location = EmptyDataProvider.class, value = EmptyDataProvider.PROVIDER_NAME)
	@Merge(location = DummyDP1.class)
	public void testMerge1NoF(DummyData d) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, N_DP1);
		
		Assert.assertTrue(DP1.contains(d));
		runTest(methodName, d);
	}
	
	@Test
	@UseDataProvider(location = EmptyDataProvider.class, value = EmptyDataProvider.PROVIDER_NAME)
	@Merge(location = DummyDP1.class, filter = F1.class)
	public void testMerge1F(DummyData d) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, N_F1DP1);
		
		Assert.assertTrue(F1DP1.contains(d));
		runTest(methodName, d);
	}
	
	@Test
	@UseDataProvider(location = DummyDP1.class, value = DummyDP1.PROVIDER_NAME)
	@Merge(location = DummyDP2.class)
	public void testUse1Merge1NoF(DummyData d) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, N_DP1 + N_DP2);
		
		Assert.assertTrue(DP1.contains(d) || DP2.contains(d));
		runTest(methodName, d);
	}
	
	@Test
	@UseDataProvider(location = EmptyDataProvider.class, value = EmptyDataProvider.PROVIDER_NAME)
	@Merge(location = DummyDP1.class, filter = F1.class)
	@Merge(location = DummyDP2.class, filter = F1.class)
	public void testMerge2F(DummyData d) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, N_F1DP1 + N_F1DP2);
		
		Assert.assertTrue(F1DP1.contains(d) || F1DP2.contains(d));
		runTest(methodName, d);
	}
	
	@Test
	@UseDataProvider(location = EmptyDataProvider.class, value = EmptyDataProvider.PROVIDER_NAME)
	@Merge(location = DummyDP1.class, filter = F1.class)
	@Merge(location = DummyDP2.class, filter = F2.class)
	public void testMerge2F2(DummyData d) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, N_F1DP1 + N_F2DP2);
		
		Assert.assertTrue(F1DP1.contains(d) || F2DP2.contains(d));
		runTest(methodName, d);
	}
	
	@Test
	@UseDataProvider(location = EmptyDataProvider.class, value = EmptyDataProvider.PROVIDER_NAME)
	@Merge(location = DummyDP1.class, limit = 20)
	@Merge(location = DummyDP2.class, limit = 5)
	public void testMerge2L(DummyData d) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, min(N_DP1, 20) + min(N_DP2, 5));
		
		Assert.assertTrue(DP1.contains(d) || DP2.contains(d));
		runTest(methodName, d);
	}
	
	@Test
	@UseDataProvider(location = EmptyDataProvider.class, value = EmptyDataProvider.PROVIDER_NAME)
	@Merge(location = DummyDP1.class, filter = F1.class, limit = 0)
	@Merge(location = DummyDP2.class, filter = F2.class, limit = 2)
	public void testMerge2FL0(DummyData d) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, min(N_F2DP2, 2));
		
		Assert.assertTrue(F2DP2.contains(d));
		runTest(methodName, d);
	}
	
	@Test
	@UseDataProvider(location = EmptyDataProvider.class, value = EmptyDataProvider.PROVIDER_NAME)
	@Merge(location = DummyDP1.class, filter = F1.class, limit = 4)
	@Merge(location = DummyDP2.class, filter = F2.class, limit = 3)
	public void testMerge2FL(DummyData d) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, min(N_F1DP1, 4) + min(N_F2DP2, 3));
		
		Assert.assertTrue(F1DP1.contains(d) || F2DP2.contains(d));
		runTest(methodName, d);
	}
	
	@Test
	@UseDataProvider(location = EmptyDataProvider.class, value = EmptyDataProvider.PROVIDER_NAME)
	@Merge(location = DummyDP1.class, filter = F1.class, limit = 4)
	@Merge(location = DummyDP2.class, filter = F2.class, limit = 3)
	@DataProviderFilter
	public void testMerge2FLFDefault(DummyData d) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, min(N_F1DP1, 4) + min(N_F2DP2, 3));
		
		Assert.assertTrue(F1DP1.contains(d) || F2DP2.contains(d));
		runTest(methodName, d);
	}
	
	@Test
	@UseDataProvider(location = EmptyDataProvider.class, value = EmptyDataProvider.PROVIDER_NAME)
	@Merge(location = DummyDP1.class, filter = F1.class, limit = 10)
	@Merge(location = DummyDP2.class, filter = F2.class, limit = 7)
	@DataProviderFilter(limit=5)
	public void testMerge2FLL(DummyData d) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, min(min(N_F1DP1, 10) + min(N_F2DP2, 7), 5));
		
		Assert.assertTrue(F1DP1.contains(d) || F2DP2.contains(d));
		runTest(methodName, d);
	}
	
	@Test
	@UseDataProvider(location = EmptyDataProvider.class, value = EmptyDataProvider.PROVIDER_NAME)
	@Merge(location = DummyDP1.class, filter = F1.class, limit = 6)
	@Merge(location = DummyDP2.class, filter = F1.class, limit = 7)
	@DataProviderFilter(filter=F2.class)
	public void testMerge2FLF(DummyData d) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, fTl(F2.class, -1,  fTl(F1.class, 6, DP1), fTl(F1.class, 7, DP2)).size());
		
		Assert.assertTrue(F2F1DP1.contains(d) || F2F1DP2.contains(d));
		runTest(methodName, d);
	}
	
	@Test
	@UseDataProvider(location = EmptyDataProvider.class, value = EmptyDataProvider.PROVIDER_NAME)
	@Merge(location = DummyDP1.class, filter = F1.class, limit = 2)
	@Merge(location = DummyDP2.class, filter = F1.class, limit = 3)
	@DataProviderFilter(filter=F2.class, limit=5)
	public void testMerge2FLFL(DummyData d) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, fTl(F2.class, 5,  fTl(F1.class, 2, DP1), fTl(F1.class, 3, DP2)).size());
		
		Assert.assertTrue(F2F1DP1.contains(d) || F2F1DP2.contains(d));
		runTest(methodName, d);
	}
	
	@Test
	@UseDataProvider(location = DummyDP1.class, value = DummyDP1.PROVIDER_NAME)
	@Merge(location = DummyDP1.class, filter = F1.class, limit = 5)
	@Merge(location = DummyDP1.class, filter = F2.class, limit = 4)
	public void testMergeSame3FL(DummyData d) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, N_DP1 + min(N_F1DP1, 5) + min(N_F2DP1, 4));
		
		Assert.assertTrue(DP1.contains(d));
		runTest(methodName, d);
	}
	

/* ******************************************************
 *  Test @AddIntParams / @AddStringParams / @AddParams
 * ******************************************************/
	
	@Test
	@UseDataProvider(location = DummyDP1.class, value = DummyDP1.PROVIDER_NAME)
	@DataProviderFilter(limit=10)
	@AddIntParams({ 1,2,3 })
	public void testEmptyPi(DummyData d, int p1) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, 3*min(N_DP1,10));
		
		Assert.assertTrue(DP1.contains(d));
		Assert.assertTrue(Arrays.asList(1,2,3).contains(p1));
		runTest(methodName, d);
	}
	
	@Test
	@UseDataProvider(location = DummyDP1.class, value = DummyDP1.PROVIDER_NAME)
	@DataProviderFilter(limit=10)
	@AddIntParams({ 1,2,3 })
	@AddIntParams(4)
	public void testEmptyPiPi(DummyData d, int p1, int p2) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, 1*3*min(N_DP1,10));
		
		Assert.assertTrue(DP1.contains(d));
		Assert.assertTrue(Arrays.asList(1,2,3).contains(p1));
		Assert.assertTrue(Arrays.asList(4).contains(p2));
		runTest(methodName, d);
	}
	
	@Test
	@UseDataProvider(location = DummyDP1.class, value = DummyDP1.PROVIDER_NAME)
	@DataProviderFilter(limit=10)
	@AddStringParams("a")
	public void testEmptyPs(DummyData d, String p1) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, 1*min(N_DP1,10));
		
		Assert.assertTrue(DP1.contains(d));
		Assert.assertTrue(Arrays.asList("a").contains(p1));
		runTest(methodName, d);
	}
	
	@Test
	@UseDataProvider(location = DummyDP1.class, value = DummyDP1.PROVIDER_NAME)
	@DataProviderFilter(limit=10)
	@AddStringParams({"a", "b"})
	@AddStringParams({"c", "d"})
	public void testEmptyPsPs(DummyData d, String p1, String p2) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, 2*2*min(N_DP1,10));
		
		Assert.assertTrue(DP1.contains(d));
		Assert.assertTrue(Arrays.asList("a", "b").contains(p1));
		Assert.assertTrue(Arrays.asList("c", "d").contains(p2));
		runTest(methodName, d);
	}
	
	@Test
	@UseDataProvider(location = EmptyDataProvider.class, value = EmptyDataProvider.PROVIDER_NAME)
	@Merge(location = DummyDP1.class, filter = F1.class, limit = 9)
	@Merge(location = DummyDP2.class, filter = F1.class, limit = 10)
	@DataProviderFilter(filter=F2.class, limit=5)
	@AddStringParams({"a", "b"})
	@AddIntParams(1)
	@AddStringParams({"c"})
	@AddIntParams({2,3})
	public void testMerge2FLFLPiPiPsPs(DummyData d, int p1, int p2, String p3, String p4) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, 2*1*1*2 * fTl(F2.class, 5, fTl(F1.class, 9, DP1), fTl(F1.class, 10, DP2)).size());
		
		Assert.assertTrue(F2F1DP1.contains(d) || F2F1DP2.contains(d));
		Assert.assertTrue(Arrays.asList(1).contains(p1));
		Assert.assertTrue(Arrays.asList(2,3).contains(p2));
		Assert.assertTrue(Arrays.asList("a", "b").contains(p3));
		Assert.assertTrue(Arrays.asList("c").contains(p4));
		runTest(methodName, d);
	}
	
	@Test
	@UseDataProvider(location = DummyDP1.class, value = DummyDP1.PROVIDER_NAME)
	@DataProviderFilter(limit=10)
	@AddParams(location = DummyDP1.class, limit = 2)
	public void testDP1LParamedByDP1L(DummyData d, DummyData p1) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, min(N_DP1,2)*min(N_DP1,10));
		
		Assert.assertTrue(DP1.contains(d));
		Assert.assertTrue(DP1.contains(p1));
		runTest(methodName, d);
	}
	
	@Test
	@UseDataProvider(location = DummyDP1.class, value = DummyDP1.PROVIDER_NAME)
	@DataProviderFilter(limit=5)
	@AddParams(location = DummyDP2.class, filter = F1.class, limit = 3)
	public void testDP1LParamedByDP2FL(DummyData d, DummyData p1) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, min(N_F1DP2,3)*min(N_DP1,5));
		
		Assert.assertTrue(DP1.contains(d));
		Assert.assertTrue(F1DP2.contains(p1));
		runTest(methodName, d);
	}
	
	@Test
	@UseDataProvider(location = EmptyDataProvider.class, value = EmptyDataProvider.PROVIDER_NAME)
	@Merge(location = DummyDP1.class, filter = F1.class, limit = 2)
	@Merge(location = DummyDP2.class, filter = F1.class, limit = 3)
	@DataProviderFilter(filter = F2.class, limit = 5)
	@AddParams(location = DummyDP1.class, limit = 2)	
	@AddParams(location = DummyDP2.class, filter = F1.class, limit = 3)
	public void testMerge2FLFLPoPo(DummyData d, DummyData p1, DummyData p2) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, min(N_DP1,2) * min(N_F1DP2,3) * fTl(F2.class, 5, fTl(F1.class, 2, DP1), fTl(F1.class, 3, DP2)).size());
		
		Assert.assertTrue(F2F1DP1.contains(d) || F2F1DP2.contains(d));
		Assert.assertTrue(DP1.contains(p1));
		Assert.assertTrue(F1DP2.contains(p2));
		runTest(methodName, d);
	}
	
	@Test
	@UseDataProvider(location = EmptyDataProvider.class, value = EmptyDataProvider.PROVIDER_NAME)
	@Merge(location = DummyDP1.class, filter = F1.class, limit = 4)
	@Merge(location = DummyDP2.class, filter = F1.class, limit = 6)
	@DataProviderFilter(filter=F2.class, limit=5)
	@AddIntParams(1)
	@AddIntParams({2,3})
	@AddStringParams({"a", "b"})
	@AddStringParams("c")
	@AddParams(location = DummyDP1.class, limit = 2)
	@AddParams(location = DummyDP2.class, filter = F1.class, limit = 3)
	public void testMerge2FLFLPiPiPsPsPoPo1(DummyData d, int p1, int p2, String p3, String p4, DummyData p5, DummyData p6) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, 1 * 2 * 2 * 1 * min(N_DP1,2) * min(N_F1DP2,3) * fTl(F2.class, 5, fTl(F1.class, 4, DP1), fTl(F1.class, 6, DP2)).size());
		
		Assert.assertTrue(F2F1DP1.contains(d) || F2F1DP2.contains(d));
		Assert.assertTrue(Arrays.asList(1).contains(p1));
		Assert.assertTrue(Arrays.asList(2,3).contains(p2));
		Assert.assertTrue(Arrays.asList("a", "b").contains(p3));
		Assert.assertTrue(Arrays.asList("c").contains(p4));
		Assert.assertTrue(DP1.contains(p5));
		Assert.assertTrue(F1DP2.contains(p6));
		runTest(methodName, d);
	}
	
	@Test
	@UseDataProvider(location = EmptyDataProvider.class, value = EmptyDataProvider.PROVIDER_NAME)
	@Merge(location = DummyDP1.class, filter = F1.class, limit = 4)
	@Merge(location = DummyDP2.class, filter = F1.class, limit = 6)
	@DataProviderFilter(filter=F2.class, limit=5)
	@AddIntParams(1)
	@AddIntParams({2,3})
	@AddStringParams({"a", "b"})
	@AddStringParams("c")
	@AddParams(location = DummyDP1.class, limit = 2)
	@AddParams(location = DummyDP2.class, filter = F1.class, limit = 3)
	public void testMerge2FLFLPiPiPsPsPoPo2(Object... params) {
		DummyData d = (DummyData) params[0];
		int p1 = (int) params[1];
		int p2 = (int) params[2];
		String p3 = (String) params[3];
		String p4 = (String) params[4];
		DummyData p5 = (DummyData) params[5];
		DummyData p6 = (DummyData) params[6];
		
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, 1 * 2 * 2 * 1 * min(N_DP1,2) * min(N_F1DP2,3) * fTl(F2.class, 5, fTl(F1.class, 4, DP1), fTl(F1.class, 6, DP2)).size());
		
		Assert.assertTrue(F2F1DP1.contains(d) || F2F1DP2.contains(d));
		Assert.assertTrue(Arrays.asList(1).contains(p1));
		Assert.assertTrue(Arrays.asList(2,3).contains(p2));
		Assert.assertTrue(Arrays.asList("a", "b").contains(p3));
		Assert.assertTrue(Arrays.asList("c").contains(p4));
		Assert.assertTrue(DP1.contains(p5));
		Assert.assertTrue(F1DP2.contains(p6));
		runTest(methodName, d);
	}
	
	@Test
	@UseDataProvider(location = EmptyDataProvider.class, value = EmptyDataProvider.PROVIDER_NAME)
	@Merge(location = DummyDP1.class, filter = F1.class, limit = 4)
	@Merge(location = DummyDP2.class, filter = F1.class, limit = 6)
	@DataProviderFilter(filter=F2.class, limit=5)
	@AddParams(location = DummyDP1.class, limit = 2)
	@AddStringParams({"a", "b"})
	@AddIntParams(1)
	@AddStringParams("c")
	@AddParams(location = DummyDP2.class, filter = F1.class, limit = 3)
	@AddIntParams({2,3})
	public void testMerge2FLFLPiPiPsPsPoPo3(DummyData d, int p1, int p2, String p3, String p4, DummyData p5, DummyData p6) {
		Method methodName = new Object(){}.getClass().getEnclosingMethod();
		initExpected(methodName, 1 * 2 * 2 * 1 * min(N_DP1,2) * min(N_F1DP2,3) * fTl(F2.class, 5, fTl(F1.class, 4, DP1), fTl(F1.class, 6, DP2)).size());
		
		Assert.assertTrue(F2F1DP1.contains(d) || F2F1DP2.contains(d));
		Assert.assertTrue(Arrays.asList(1).contains(p1));
		Assert.assertTrue(Arrays.asList(2,3).contains(p2));
		Assert.assertTrue(Arrays.asList("a", "b").contains(p3));
		Assert.assertTrue(Arrays.asList("c").contains(p4));
		Assert.assertTrue(DP1.contains(p5));
		Assert.assertTrue(F1DP2.contains(p6));
		runTest(methodName, d);
	}
	
	
/* ******************************************************
 *  Dummy Data / Providers / Filters 
 * ******************************************************/
	
	public static List<DummyData> filterAll(List<DummyData> data, Class<? extends IDataFilter<DummyData>> filter) {
		Stream<DummyData> stream = data.stream();
		return AbstractDataProvider.filter(stream, filter).collect(Collectors.toList());
	}
	
	@SafeVarargs
	static List<DummyData> fTl(Class<? extends IDataFilter<DummyData>> filter, int limit, List<DummyData>... dataList) {
		return AbstractDataProvider.filterThenLimit(Arrays.stream(dataList).flatMap(l -> l.stream()), filter, limit).collect(Collectors.toList());
	}
	
	static class DummyData implements IData {
		String name;
		
		public DummyData(String name) { 
			this.name = name; 
		}
		
		@Override 
		public String getName() { 
			return name; 
		}
		
		@Override
		public String toString() {
			return name;
		}
	}
	
	public static class DummyDP1 extends AbstractDataProvider<DummyData> {
		
		@DataProvider 
		public static List<List<Object>> dataProvider(FrameworkMethod method) {
			return new DummyDP1().provide(method);
		}
		
		@Override
		protected Stream<DummyData> createDataStream(FrameworkMethod method) {
			return data.stream();
		}
		
		static List<DummyData> data = Arrays.asList (
			new DummyData("10a"),
			new DummyData("11b"),
			new DummyData("12c"),
			new DummyData("13d"),
			new DummyData("14abc"),
			new DummyData("REWG2ergds"),
			new DummyData("16akfjgafkgja"),
			new DummyData("17herahz"),
			new DummyData("18@%!@#%@%"),
			new DummyData("1915325"),
			new DummyData("q3gf2dQERT"),
			new DummyData("SDGSG"),
			new DummyData("TRWEHRQ")
		);
	}
	
	public static class DummyDP2 extends AbstractDataProvider<DummyData> {
		
		@DataProvider 
		public static List<List<Object>> dataProvider(FrameworkMethod method) {
			return new DummyDP2().provide(method);
		}
		
		@Override
		protected Stream<DummyData> createDataStream(FrameworkMethod method) {
			return data.stream();
		}
		
		static List<DummyData> data = Arrays.asList (
			new DummyData("20zgd"),
			new DummyData("21]dfb"),
			new DummyData("22reg"),
			new DummyData("2343gsdv"),
			new DummyData("24r54y"),
			new DummyData("255yn zde"),
			new DummyData("26.a,/ger"),
			new DummyData("fw3fF$#RF"),
			new DummyData("iu34tg;aej"),
			new DummyData("235"),
			new DummyData("lakfgh"),
			new DummyData("23jrbdf"),
			new DummyData("ATKQ#$t"),
			new DummyData("AWERG#$T"),
			new DummyData("Q#$T#Qgdfg")
		);
	}
	
	public static class F1 implements IDataFilter<DummyData> {
		public boolean test(DummyData data) {
			return data.name.contains("a") || data.name.length() > 6;
		}
	}
	
	public static class F2 implements IDataFilter<DummyData> {
		public boolean test(DummyData data) {
			return data.name.contains("2") || data.name.length() < 4 ;
		}
	}

}
