# Changelog

## Release version 2.1.0 (18 mai 2018)

* [Update] gecos-framework to v1.3.0
* [CI] gitlab-ci can now use variable 'JENKINS\_FAIL\_MODE' to specify the behaviour depending on test results

---

## Release version 2.0.0 (22 mars 2018)

Redesign test template model, data providers  and major refactoring:

* [Change] `AbstractTestTemplate` to allow for specifying a custom test stages.
* [Add] `TestFlowTemplate`, a `AbstractTestTemplate` that supports dynamically specifyied (multiple) 
  testFlows for different data classes.
* [Add] `S2STestTemplate` and `S2STestFlow` as default implemetation of `TestFlowTemplate`/`TestFlow` 
  with standard source-to-source stages.
* [Add] utility classes for creating stages and common operations such as compile/link/compare etc.
* [Add] `TestFlow` and `S2STestFlow` builders.
* [Change] replace old generator, compilor, codegenerator etc.  with `IVersionOperation` and `IVersionBiOperation` etc.
* [Add] more methods to handle annotations in `IVersion`.
* [Add] Support for limiting the number of provider data from any `AbstractDataProvider`.
* [Add] Support for filtering data from any `AbstractDataProvider`.
* [Add] Support for merging data from any `AbstractDataProvider`.
* [Add] Support for adding additional test method parameters using `@AddXParams` from any `AbstractDataProvider`.
* [Add] new Data types:
  - `CxxFileData`: a regular file names '\*.c' or '\*cpp' or '\*.cc'.
  - `CxxFilesGroupData`: a folder named '\*.group' with optionnaly a makefile in it.
  - `CxxMakefileData`: a folder containing a '[M/m]akefile' file.
* [Add] dataClasses attribut to `ResourcesLocation` annotation to specify the type of data to locate.
  It deafults to [`CxxFilesGroupData`, `CxxMakefileData`, `CxxFileData`].
* [Remove] `MultipleDataProvider`: replaced by `AbstractDataProvider`.
* [Remove] support for data provider generators and all generated providers.
* [Remove] `IDataFactory`: replaced by static methods in each `AbstractDataFromPath`.
- [Remove] `IVersion`.generatedMakefile and rename rootMakefile as makefile.
- [Change] `IVersion`.setCodegenDir now also create all parent dirs if needed.
- [Change] replaced the use of Rules in `AbstractTestTemplate` (rules only work when run with junit).
* [Change] Use 'Path' instead of 'File'.
* [Change] renamed Data, annotations, exceptions ..
* [Change] refactor (merge/add/remove) packages.
* [Improv] error reporting.
* [Update] gecos-framework to release version v1.2.0.

---


## Release version 1.0.2 (02 févr. 2018)

* [CI] update gecos-buildtools to latest.
* [Add] `IDataFactory` and methods to `DataProviderUtils` to find `IData` instances using the factories
* [Remove] accidently added generated dataprovider.
* [Change] default test generated files location to './target/test-regen/CLASSNAME'.

---


## Release version 1.0.1 (17 Oct 2017)

### Changes
* CI now uses submodule 'gecos-buildtools', and delegate
  build/test to Jenkins.

### Fixes
* add testframework.resources/tools to the binary build to allow
  other plugins to discover it from jar.
* replaced composite eclipse oxygen update site with fixed build-specific site.

---


## Release version 1.0.0 (24 Jul 2017)

Initial version.

Migrated GeCoS Test Framework plugins from SVN repository at
*/trunk/gecos/test-utils (rev 17802)*

### Changes
* rename _fr.irisa.cairn.gecos.tests.framework_ as _fr.irisa.cairn.gecos.testframework.model_.
* rename _fr.irisa.cairn.gecos.tests.benchmarks_ as _fr.irisa.cairn.gecos.testframework.resources_.
* rename _fr.irisa.cairn.gecos.tests.framework.unittest_ as fr.irisa.cairn.gecos.testframework.unit.test_.
* remove dependency on 'gecos-core'.
 depending parts are moved to _gecos-core/bundles/fr.irisa.cairn.gecos.core.testframework.impl_.
* move 'ResourceLocator' to _testframework.model_.
* move DataProvider generator to _testframework.model_.

### ADD
* add 'DataFromFileProvider'.
* add 'TestDataFromFile' unit tests.

### New features
* 'ResourcesLocator' now can locate and extract resources from packaged bundles (jar).
* Support for automatically providing data from a specified directory within
 a given bundle. Use 'DataFromFileProvider' with annotations '@ResourcesLocation'.

