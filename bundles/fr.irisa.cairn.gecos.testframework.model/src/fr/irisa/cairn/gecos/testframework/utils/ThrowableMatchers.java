package fr.irisa.cairn.gecos.testframework.utils;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.Is;

public class ThrowableMatchers {
	
	private ThrowableMatchers() {}

	private static class IsAndHasCause<T extends Throwable> extends TypeSafeMatcher<T> {
		private final Matcher<? extends Throwable> isMatcher;
		private final Matcher<? extends Throwable> causeMatcher;
		
		/**
		 * Instantiates a new checks if is and has cause.
		 *
		 * @param isMatcher the is matcher
		 * @param causeMatcher the cause matcher
		 */
		private IsAndHasCause(Matcher<? extends Throwable> isMatcher, Matcher<? extends Throwable> causeMatcher) {
			this.isMatcher = isMatcher;
			this.causeMatcher = causeMatcher;
	    }
		
		/* (non-Javadoc)
		 * @see org.hamcrest.TypeSafeMatcher#matchesSafely(java.lang.Object)
		 */
		@Override
		protected boolean matchesSafely(T item) {
			 return isMatcher.matches(item) && causeMatcher.matches(item.getCause());
		}
		
		/* (non-Javadoc)
		 * @see org.hamcrest.SelfDescribing#describeTo(org.hamcrest.Description)
		 */
		@Override
		public void describeTo(Description description) {
			description.appendText("exception is an instance of ");
			description.appendDescriptionOf(isMatcher);
			description.appendText(" with cause ");
			description.appendDescriptionOf(causeMatcher);
		}
	}
	
	/**
	 * Checks if is and has cause.
	 *
	 * @param class1 the class 1
	 * @param class2 the class 2
	 * @return the matcher
	 */
	public static Matcher<Throwable> isAndHasCause(Class<? extends Throwable> class1, Class<? extends Throwable> class2) {
		return new IsAndHasCause<>(Is.isA(class1), Is.isA(class2));
	}
	
}
