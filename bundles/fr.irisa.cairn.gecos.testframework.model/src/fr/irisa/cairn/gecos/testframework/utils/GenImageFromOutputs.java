package fr.irisa.cairn.gecos.testframework.utils;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import javax.imageio.ImageIO;

//XXX
public class GenImageFromOutputs {
	
	/**
	 * Generate.
	 *
	 * @param output_file the output file
	 */
	public static void generate(String output_file) {
		BufferedReader outFile;
		try {
			outFile = new BufferedReader(new FileReader(output_file));
		
			long nDims = Long.valueOf(outFile.readLine());
			if(nDims != 2) {
				outFile.close();
				throw new RuntimeException("Invalid File format: first line must contain the number of dimension and should be equal 2!");
			}
			int height = (int) Float.valueOf(outFile.readLine()).longValue();//XXX
			int width = (int) Float.valueOf(outFile.readLine()).longValue();
			
			BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
			WritableRaster img_data = img.getRaster();
			
			for(int x=0; x<height; x++) {
				for(int y=0; y<width; y++) {
//					img.setRGB(x, y, Integer.valueOf(outFile.readLine()));
					Integer pixel = (int) Float.valueOf(outFile.readLine()).longValue();
					img_data.setPixel(x, y, new int[] {pixel});
				}
			}
			outFile.close();
			
			File imgFile = new File(output_file+".bmp");
			ImageIO.write(img, "bmp", imgFile);
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
