package fr.irisa.cairn.gecos.testframework.model;

import java.util.function.Predicate;

import fr.irisa.cairn.gecos.testframework.stages.SequenceStage;
import fr.irisa.cairn.gecos.testframework.stages.Stages;

@FunctionalInterface
public interface ITestStage {

	void apply(Object target);
	
	final class Tag { String value = ""; }
	
	final Tag tag = new Tag();

	/**
	 * Set the tag of {@code this} stage to the specified one.
	 * 
	 * @param newTag tag value. If {@code null} an empty String is used instead.
	 * @return {@code this} stage.
	 */
	public default ITestStage setTag(String newTag) {
		tag.value = newTag == null ? "" : newTag;
		return this;
	}
	
	/**
	 * @return the tag value of {@code this} stage. Never {@code null}.
	 */
	public default String getTag() {
		return tag.value;
	}
	
	/**
	 * Create a new stage sequence containing {@code this}
	 * followed by the specified {@code stage} (this then stage).
	 * 
	 * @param stage a stage before which {@code this} is inserted.
	 * @return a new stage sequence.
	 */
	public default SequenceStage then(ITestStage stage) {
		return Stages.chain(this, stage);
	}
	
	/**
	 * Create a new stage sequence containing {@code this} followed by a 
	 * {@link Stages#onlyIf(Predicate, ITestStage) conditional} 
	 * stage that applies the specified {@code stage} only if the specified
	 * predicate evaluates to {@code true} when the returned stage is applied.
	 * 
	 * @param condition test the given test template.
	 * @param stage a stage before which {@code this} is inserted.
	 * @return a new stage sequence.
	 */
	public default SequenceStage thenIf(Predicate<Object> condition, ITestStage stage) {
		return then(Stages.onlyIf(condition, stage));
	}
	
	/**
	 * Create a new {@link Stages#onlyIf(Predicate, ITestStage) conditional} 
	 * stage that applies {@code this} only if the specified predicate evaluates to 
	 * {@code true} when the returned stage is applied.
	 * 
	 * @param condition test the given test template.
	 * @return a new conditional stage.
	 */
	public default ITestStage onlyIf(Predicate<Object> condition) {
		return Stages.onlyIf(condition, this);
	}
	
	/**
	 * Create a new {@link Stages#branch(Predicate, ITestStage, ITestStage) branch} 
	 * stage that applies {@code this} only if the specified predicate evaluates to 
	 * {@code true}, when the returned stage is applied, Otherwise it applies
	 * the specified {@code orElse} stage instead.
	 * 
	 * @param condition test the given test template.
	 * @return a new branch stage.
	 */
	public default ITestStage onlyIf(Predicate<Object> condition, ITestStage orElse) {
		return Stages.branch(condition, this, orElse);
	}
	
}