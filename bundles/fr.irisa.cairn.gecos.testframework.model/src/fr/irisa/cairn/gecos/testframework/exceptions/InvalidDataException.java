package fr.irisa.cairn.gecos.testframework.exceptions;

public class InvalidDataException extends Exception {

	/**
	 * Instantiates a new invalid data exception.
	 *
	 * @param msg the msg
	 */
	public InvalidDataException(String msg) {
		super(msg);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8740512042700764649L;

}
