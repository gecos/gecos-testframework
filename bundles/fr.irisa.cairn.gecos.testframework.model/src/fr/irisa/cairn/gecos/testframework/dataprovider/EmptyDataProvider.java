package fr.irisa.cairn.gecos.testframework.dataprovider;

import java.util.List;
import java.util.stream.Stream;

import org.junit.runners.model.FrameworkMethod;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.testframework.model.IData;

/**
 * This {@link AbstractDataProvider} implementation provides no data.
 * It is intended to be use with the {@literal @}{@link UseDataProvider}
 * so that only data from {@literal @} {@link Merge} annotations are used
 * for example.
 * 
 * @author aelmouss
 */
public class EmptyDataProvider extends AbstractDataProvider<IData>  {
	
	@DataProvider 
	public static List<List<Object>> dataProvider(FrameworkMethod method) {
		return new EmptyDataProvider().provide(method);
	}
	
	@Override
	protected Stream<IData> createDataStream(FrameworkMethod method) {
		return Stream.of();
	}

}
