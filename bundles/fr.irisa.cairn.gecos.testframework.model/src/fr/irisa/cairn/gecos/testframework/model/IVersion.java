package fr.irisa.cairn.gecos.testframework.model;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * 
 * @author aelmouss
 */
public abstract class IVersion {

	protected String nameId;
	protected IVersion previous = null;
	
	/**
	 * Sets the name.
	 *
	 * @param nameId the new name
	 */
	public void setName(String nameId) {
		this.nameId = nameId;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return nameId;
	}

	/**
	 * Sets the previous.
	 *
	 * @param previousVersion the new previous
	 */
	public void setPrevious(IVersion previousVersion) {
		this.previous = previousVersion;
	}
	
	/**
	 * Gets the previous.
	 *
	 * @return the previous
	 */
	public IVersion getPrevious() {
		return previous;
	}
	
	/**
	 * Checks if is root.
	 *
	 * @return true, if is root
	 */
	public boolean isRoot() {
		return previous == null;
	}

	/**
	 * Gets the root version.
	 *
	 * @return the root version by walking {@link #getPrevious()} links.
	 */
	public IVersion getRootVersion() {
		if(previous == this)
			throw new RuntimeException("Invalid version: has a cycle! " + this);
		if(isRoot())
			return this;
		return previous.getRootVersion();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return nameId;
	}

/* ************************
 *  Should be provided before code generation
 * ************************/
	/**
	 * The directory where the generated files will be placed.
	 */
	protected Path codegenDir;
	
	/**
	 * Additional sources needed to link this version.
	 * May also be needed for code generation. 
	 */
	protected Collection<Path> externalSources;
	
	/**
	 * Gets the codegen dir.
	 *
	 * @return the codegen dir
	 */
	public Path getOutputDir() {
		return codegenDir;
	}
	
	/**
	 * Sets and create the codegen dir.
	 *
	 * @param codegenDir the new codegen dir
	 */
	public void setCodegenDir(Path codegenDir) {
		try {
			Files.createDirectories(codegenDir);
			this.codegenDir = codegenDir;
		} catch (IOException e) {
			throw new RuntimeException("Error while creating new folder: " + codegenDir ,e);
		}
	}
	
	/**
	 * Gets the external source files.
	 *
	 * @return the external source files
	 */
	//TODO remove : handled by ICompiler
	public Collection<Path> getExternalSourceFiles() {
		return this.externalSources;
	}
	
	/**
	 * Sets the external sources.
	 *
	 * @param externalSources the new external sources
	 */
	public void setExternalSources(Collection<Path> externalSources) {
		this.externalSources = externalSources;
	}
	
/* ************************
 *  Should be provided before Compiling
 * ************************/
	/**
	 * List of all source files used to compile/link the code by the {@link ICompiler}.
	 * This may contain files that are not included in {@link #gProject}.
	 */
	protected List<Path> sourceFiles;
	
	/**
	 * Gets the generated source files.
	 *
	 * @return the generated source files
	 */
	public List<Path> getGeneratedSourceFiles() {
		return sourceFiles;
	}
	
	/**
	 * Sets the generated source files.
	 *
	 * @param sourceFiles the new generated source files
	 */
	public void setGeneratedSourceFiles(List<Path> sourceFiles) {
		this.sourceFiles = sourceFiles;
	}
	
	/**
	 * List of all include directories used to compile/link the code by the {@link ICompiler}.
	 * This may contain files that are not included in {@link #gProject}.
	 */
	protected List<Path> includeDirs;
	
	/**
	 * Gets the include dirs.
	 *
	 * @return the include dirs
	 */
	public List<Path> getIncludeDirs() {
		return includeDirs;
	}
	
	/**
	 * Sets the include dirs.
	 *
	 * @param includeDirs the new include dirs
	 */
	public void setIncludeDirs(List<Path> includeDirs) {
		this.includeDirs = includeDirs;
	}

	
/* ************************
 *  Should be provided before Linking
 * ************************/
	
	protected List<Path> generatedObjects;
	
	/**
	 * @return list of generated Object files
	 */
	public List<Path> getGeneratedObjects() {
		return generatedObjects;
	}
	
	/**
	 * Set the list of generated objects
	 * 
	 * @param generatedObjects list
	 */
	public void setGeneratedObjects(List<Path> generatedObjects) {
		this.generatedObjects = generatedObjects;
	}
	
	/**
	 * The executable file obtained by compiling/linking this version.
	 */
	protected Path executable;
	
	/**
	 * Gets the executable.
	 *
	 * @return the executable
	 */
	public Path getExecutable() {
		return this.executable;
	}

	/**
	 * Sets the executable.
	 *
	 * @param executable the new executable
	 */
	public void setExecutable(Path executable) {
		this.executable = executable;
	}
	
/* ************************
 *  Should be set by the Executor
 * ************************/
	/**
	 * The exit code obtained by executing {@link #executable}.
	 * Should be set by the {@link IExecutor}.
	 */
	protected Integer exitCode;
	
	/**
	 * if not @{code null}, the stdout of the execution command should be directed here.
	 */
	protected Path stdoutFile;
	
	/**
	 * if not @{code null}, the stderr of the execution command should be directed here.
	 */
	protected Path stderrFile;
	
	
	/**
	 * Gets the exit code.
	 *
	 * @return the exit code
	 */
	public Integer getExitCode() {
		return exitCode;
	}
	
	/**
	 * Sets the exit code.
	 *
	 * @param exitCode the new exit code
	 */
	public void setExitCode(int exitCode) {
		this.exitCode = exitCode;
	}
	
	/**
	 * Sets the stdout file.
	 *
	 * @param stdoutFile the new stdout file
	 */
	public void setStdoutFile(Path stdoutFile) {
		this.stdoutFile = stdoutFile;
	}
	
	/**
	 * Sets the stderr file.
	 *
	 * @param stderrFile the new stderr file
	 */
	public void setStderrFile(Path stderrFile) {
		this.stderrFile = stderrFile;
	}
	
	/**
	 * Gets the stdout file.
	 *
	 * @return the stdout file
	 */
	public Path getStdoutFile() {
		return stdoutFile;
	}
	
	/**
	 * Gets the stderr file.
	 *
	 * @return the stderr file
	 */
	public Path getStderrFile() {
		return stderrFile;
	}
	
/* ************************
 *  In case a makefile is provided
 * ************************/
	
	protected Path makefile;
	
	public Path getMakefile() {
		return makefile;
	}
	
	public void setMakefile(Path makefile) {
		this.makefile = makefile;
	}
	
	
/* ************************
 * Additional objects can be attached to a version 
 * ************************/
	
	private Map<String,Object> annotations;
	
	/**
	 * Gets the arguments.
	 *
	 * @return the arguments
	 */
	public Map<String, Object> getAnnotations() {
		if(annotations == null)
			annotations = new LinkedHashMap<>();
		return annotations;
	}
	
	/**
	 * Adds a annotation.
	 *
	 * @param key
	 * @param value
	 */
	public void addAnnotation(String key, Object value) {
		getAnnotations().put(key, value);
	}
	
	/**
	 * Adds a annotation only if none with the specified
	 * key does not already exists. Do nothing otherwise.
	 *
	 * @param key the key
	 * @param value the arg
	 */
	public void addAnnotationIfNew(String key, Object value) {
		if(!getAnnotations().containsKey(key))
			getAnnotations().put(key, value);
	}
	
	/**
	 * Try to get annotation for key as T.
	 * If such annotation does not exist or is not compatible with T,
	 * {@code null} is returned.
	 * 
	 * @param <T> annotation type
	 * @param key annotation key
	 * @return annotation for key or {@code null} if not found/compatible.
	 */
	@SuppressWarnings("unchecked")
	public <T> T getAnnotation(String key) {
		try {
			return (T) getAnnotations().get(key);
		} catch(Exception e) {
			return null;
		}
	}

	/**
	 * Timeout in seconds used by {@link ICompiler}, {@link IExecutor} ...
	 * If {@code null}, no timeout is applied.
	 */
	private Long timeout;
	
	/**
	 * Gets the timeout.
	 *
	 * @return the timeout
	 */
	public Long getTimeout() {
		return timeout;
	}
	
	/**
	 * Sets the timeout.
	 *
	 * @param timeout the new timeout
	 */
	public void setTimeout(Long timeout) {
		this.timeout = timeout;
	}

}