package fr.irisa.cairn.gecos.testframework.exceptions;

public class ComparatorFailure extends OperationFailureException {

	/**
	 * Instantiates a new comparator failure exception.
	 *
	 * @param msg the msg
	 */
	public ComparatorFailure(String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new comparator failure exception.
	 *
	 * @param string the string
	 * @param e the e
	 */
	public ComparatorFailure(String string, Exception e) {
		super(string, e);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8740512042700764649L;

}
