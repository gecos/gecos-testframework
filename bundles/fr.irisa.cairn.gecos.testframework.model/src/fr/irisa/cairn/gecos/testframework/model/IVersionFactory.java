package fr.irisa.cairn.gecos.testframework.model;

@FunctionalInterface
public interface IVersionFactory<V extends IVersion> {

	/**
	 * Creates a new IVersion object.
	 * @return the created version.
	 */
	public V createVersion();
}
