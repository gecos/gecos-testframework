package fr.irisa.cairn.gecos.testframework.utils;

import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.file.FileSystemLoopException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.function.BiFunction;
import java.util.stream.Stream;

import fr.irisa.cairn.gecos.testframework.data.AbstractDataFromPath;
import fr.irisa.cairn.gecos.testframework.dataprovider.DataProviderFilter;

/**
 * @author aelmouss
 */
public class DataProviderUtils {
	
	private DataProviderUtils() {}
	

	public static DataProviderFilter getDataFilterAnnotation(Method method) {
		return method.getAnnotation(DataProviderFilter.class);
	}
	
	public static <T> T tryCreateInstace(Class<? extends T> cls) {
		try {
			return cls.newInstance();
		} catch (Exception e) {
			throw new RuntimeException("Couldn't create an instance of class '" + cls + "'."
					+ " It must have a public constructor with no parameters!", e);
		}
	}

	/**
	 * Lookup regular files, in {@code rootDir} recursively, 
	 * that matches the specified filename pattern.
	 *
	 * @return stream of files, never {@code null}
	 */ 
	public static Stream<Path> lookupFiles(Path rootDir, int maxDepth, String fileNamePattern) {
//		return FileUtils.getRecursiveFiles(rootDir, d->true, f -> f.getName().matches(fileNamePattern), Integer.MAX_VALUE);
		try {
			return Files.walk(rootDir, maxDepth)
				.filter(Files::isRegularFile)
				.filter(f -> f.getFileName().toString().matches(fileNamePattern));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Lookup files (infinite depth) that match the filename pattern.
	 *
	 * @return stream of files, never {@code null}
	 */ 
	public static Stream<Path> lookupFiles(Path rootDir, String fileNamePattern) {
		return lookupFiles(rootDir, Integer.MAX_VALUE, fileNamePattern);
	}
	
	/**
	 * Lookup directories, in {@code rootDir} recursively, 
	 * that matches the specified filename pattern.
	 *
	 * @return stream of files, never {@code null}
	 */ 
	public static Stream<Path> lookupDirs(Path rootDir, int maxDepth, String fileNamePattern) {
//		return FileUtils.getRecursiveFiles(rootDir, d->true, f -> f.getName().matches(fileNamePattern), Integer.MAX_VALUE);
		try {
			return Files.walk(rootDir, maxDepth)
				.filter(Files::isDirectory)
				.filter(f -> f.getFileName().toString().matches(fileNamePattern));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Lookup directories (infinite depth) that contain at least one 
	 * regular file matching the filename pattern.
	 *
	 * @return stream of files, never {@code null}
	 */ 
	public static Stream<Path> lookupDirsContainingFile(Path rootDir, String fileNamePattern) {
		return lookupFiles(rootDir, fileNamePattern)
				.map(Path::getParent)
				.filter(Objects::nonNull)
				.distinct();
	}
	

	/**
	 * Walk the specified {@code rootDir} using {@link #findCompatibleResources(Path, int, List, BiFunction)}
	 * and try to find files compatible with one of the data classes.
	 * 
	 * The data classes are tested in order i.e. the first compatible one, for a given file,
	 * is used to create the corresponding data instance. 
	 * In this case, if the file is a directory, it's subtree is skipped.
	 * 
	 * @param rootDir the directory to recursively search for compatible files
	 * @param maxDepth the maximum depth of directory levels to visit. 
	 *   Use {@link Integer#MAX_VALUE} for unlimited.
	 * @param dataClasses a list of {@link AbstractDataFromPath} Classes that define what data to look for.
	 * @param limit specify the maximum number of data to create. The file visitor is terminated after
	 * this limit is reached. No limit is applied if negative or {@link Integer#MAX_VALUE}.
	 * 
	 * @return Stream of created data
	 * @throws IOException in case any I/O error is encountered
	 */
	public static <D extends AbstractDataFromPath> List<D> createCompatibleData(final Path rootDir, int maxDepth, 
			final List<Class<? extends D>> dataClasses, int limit) throws IOException {
		
		if(limit < 0 || limit == Long.MAX_VALUE)
			return createCompatibleData(rootDir, maxDepth, dataClasses);
					
		List<D> data = new ArrayList<>(limit > 100 ? 100 : limit);
		findCompatibleResources(rootDir, maxDepth, dataClasses, (cls, file) -> {
			data.add(AbstractDataFromPath.invokeCreateFromPathMethod(cls, file));
			return data.size() >= limit;
		});
		return data;
	}
	
	/**
	 * Same as {@link #createCompatibleData(Path, int, List, int)} with no data limit.
	 */
	public static <D extends AbstractDataFromPath> List<D> createCompatibleData(final Path rootDir, int maxDepth, 
			final List<Class<? extends D>> dataTypes) throws IOException {
		
		List<D> data = new ArrayList<>();
		findCompatibleResources(rootDir, maxDepth, dataTypes, (cls, file) -> {
			data.add(AbstractDataFromPath.invokeCreateFromPathMethod(cls, file));
			return false;
		});
		return data;
	}
	
	/**
	 * Invoke {@link #findCompatibleResources(Path, int, List, BiFunction)}
	 * with a consumer that collect every compatible Data class, Path pairs.
	 */
	public static <D extends AbstractDataFromPath> Stream<Entry<Class< ? extends D>, Path>> findCompatibleResources (
			final Path rootDir, int maxDepth, final List<Class<? extends D>> dataTypes) throws IOException {
		
		Stream.Builder<Entry<Class<? extends D>, Path>> data = Stream.builder();
		findCompatibleResources(rootDir, maxDepth, dataTypes, (cls, file) -> {
			data.accept(new SimpleEntry<>(cls, file));
			return false;
		});
		return data.build();
	}
	
	/**
	 * Invoke {@link #findCompatibleResources(Path, int, List, BiFunction)}
	 * with a consumer that collect every compatible Path.
	 */
	public static <D extends AbstractDataFromPath> List<Path> findCompatibleResourcePaths (
			final Path rootDir, int maxDepth, final List<Class<? extends D>> dataTypes) throws IOException {
		
		List<Path> data = new ArrayList<>();
		findCompatibleResources(rootDir, maxDepth, dataTypes, (cls, file) -> {
			data.add(file);
			return false;
		});
		return data;
	}
	
	/**
	 * Find the first Data class that can be instantiated from the specified path.
	 * 
	 * @param dataClasses a list of {@link AbstractDataFromPath} Classes.
	 * @param path to check
	 * @return first compatible Data class or null if none found.
	 */
	public static final<D extends AbstractDataFromPath> Class<? extends D> findFirstValidDalaClass(final List<Class<? extends D>> dataClasses, Path path) {
		return dataClasses.stream()
				.filter(cls -> AbstractDataFromPath.invokeIsValidPathMethod(cls, path))
				.findFirst().orElse(null);
	}
	
	/**
	 * Walk the specified rootDir depth-first and try to find IData compatible files
	 * as specified by the dataTypes.
	 * 
	 * <p> The data classes are processed in order i.e. the first compatible Data Class, for a given file,
	 * is used and the file's subtree is skipped if it is a directory.
	 * 
	 * <p> The {@code consumer} is called to accept a matching file with the compatible Data Class
	 * that was used. The {@code consumer} can terminate the visitor by returning true, otherwise,
	 * the visitor continue normally.
	 * 
	 * <p> The {@link FileVisitor} continues when visitFileFailed except if failure reason was
	 * caused by a {@link FileSystemLoopException} in which case the visitor terminates.
	 * 
	 * @param rootDir the directory where the recursive search starts.
	 * @param maxDepth the maximum depth of directory levels to visit. 
	 *   Use {@link Integer#MAX_VALUE} for unlimited.
	 * @param dataClasses a list of {@link AbstractDataFromPath} Classes that specify what data to look for.
	 * @param consumer to consume matching path and associated Data class. 
	 * If the consumer can terminate the visitor by return true.
	 * 
	 * @throws IOException in case any I/O error is encountered
	 */
	public static <D extends AbstractDataFromPath> void findCompatibleResources(final Path rootDir, int maxDepth, 
			final List<Class<? extends D>> dataClasses,
			BiFunction<Class<? extends D>, Path, Boolean> consumer) throws IOException {
		
		Files.walkFileTree(rootDir, EnumSet.noneOf(FileVisitOption.class), maxDepth, new FileVisitor<Path>() {
			@Override
			public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
				Class<? extends D> cls = findFirstValidDalaClass(dataClasses, dir);
				if(cls != null) {
					if(consumer.apply(cls, dir)) return FileVisitResult.TERMINATE;
					return FileVisitResult.SKIP_SUBTREE;
				}
					
				// dir is not a valid IData for any of the factories => continue visiting its subtree. 
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				Class<? extends D> cls = findFirstValidDalaClass(dataClasses, file);
				if(cls != null) {
					if(consumer.apply(cls, file)) return FileVisitResult.TERMINATE;
				}
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
				exc.printStackTrace();
				if(exc instanceof FileSystemLoopException)
					return FileVisitResult.TERMINATE;
				
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				return FileVisitResult.CONTINUE;
			}
		});
	}
	
}
