package fr.irisa.cairn.gecos.testframework.data;

import java.nio.file.Path;
import java.util.List;

import fr.irisa.cairn.gecos.testframework.model.IData;

public interface ICxxProjectData extends IData {

	/**
	 * Gets the source files.
	 *
	 * @return the list of source files
	 */
	public List<Path> getSourceFiles();
	
	/**
	 * Gets the include directories.
	 *
	 * @return the list of include directories
	 */
	public List<Path> getIncludeDirs();
	
	/**
	 * Gets the makefile.
	 *
	 * @return the makefile
	 */
	public Path getMakefile();

	/**
	 * Sets the makefile.
	 *
	 * @param makefile the new makefile
	 */
	public void setMakefile(Path makefile);
	
	
	public static final class FileNamePatterns {
		public static final String C_SRC = "^\\S+\\.c$";
		public static final String CPP_SRC = "^\\S+\\.(cpp|cc)$";
		public static final String CXX_SRC = "^\\S+\\.(c|cpp|cc)$";
		public static final String HEADERFILE_NAME_PATTERN = "^\\S+\\.(h|hpp|hh)$";
		public static final String MAKEFILE = "^[mM]akefile$";
		public static final String CXX_GROUP_DIR = "^\\S+\\.group$";
	}
}
