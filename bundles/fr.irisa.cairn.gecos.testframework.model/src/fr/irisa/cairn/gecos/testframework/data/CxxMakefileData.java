package fr.irisa.cairn.gecos.testframework.data;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;

import fr.irisa.cairn.gecos.testframework.exceptions.InvalidDataException;
import fr.irisa.cairn.gecos.testframework.utils.DataProviderUtils;

/**
 * Represent a directory that contains a Cxx application with a makefile.
 * The directory MUST directly (depth 1) contain a makefile 
 * (named 'makefile' or 'Makefile').
 * 
 * <p> All source files (whose name end with '.c', '.cpp' or '.cc') in the 
 * directory (infinite depth) are added to the list of sources.
 * <p> All directories that contain at least one header file (whose name end 
 * with '.h', 'hpp' or '.hh') in the directory (infinite depth) are 
 * added to the list of include dirs.
 * 
 * @author aelmouss
 */
public class CxxMakefileData extends AbstractCxxData {
	
	public static boolean isValidPath(Path dir) {
		return dir != null && Files.isDirectory(dir) 
				&& DataProviderUtils.lookupFiles(dir, 1, FileNamePatterns.MAKEFILE).findFirst().isPresent();
	}
	
	public static CxxMakefileData createFromPath(Path dir) throws InvalidDataException {
		if(!isValidPath(dir))
			throw new InvalidDataException("Not a valid '" + CxxMakefileData.class.getSimpleName() + "' data: " + dir);
		
	
		CxxMakefileData data = new CxxMakefileData();
		data.path = dir;
		data.srcFiles = DataProviderUtils.lookupFiles(dir, FileNamePatterns.CXX_SRC).collect(Collectors.toList());
		data.incDirs = DataProviderUtils.lookupDirsContainingFile(dir, FileNamePatterns.HEADERFILE_NAME_PATTERN)
				.collect(Collectors.toList());
		data.makefile = DataProviderUtils.lookupFiles(dir, 1, FileNamePatterns.MAKEFILE).findFirst().orElse(null);
		if(data.makefile == null)
			throw new InvalidDataException("Group directory of '" + CxxMakefileData.class.getSimpleName() 
					+ "' data must contain a makefile: " + dir);
		return data;
	}
	
}
