package fr.irisa.cairn.gecos.testframework.dataprovider;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import org.junit.runners.model.FrameworkMethod;

import com.google.common.collect.Lists;

import fr.irisa.cairn.gecos.testframework.dataprovider.DataProviderFilter.NO_FILTERING;
import fr.irisa.cairn.gecos.testframework.model.IData;
import fr.irisa.cairn.gecos.testframework.model.IDataFilter;
import fr.irisa.cairn.gecos.testframework.model.IDataProvider;
import fr.irisa.cairn.gecos.testframework.utils.DataProviderUtils;

/**
 * This is the base implementation of all data providers supported by
 * gecos testframework.
 * 
 * <p> A data provider class extending this, MUST:
 * <ul>
 * <li> be publicly accessible 
 * <li> define a public constructor with no parameters
 * <li> define a public static data provider method as following:
 * <pre>
 * {@code class DataProv extends AbstractDataProvider<DataType>} {
 *	public DataProv(){}
 *
 * 	{@literal @}DataProvider 
 *	public static {@code List<List<Object>>} {@value #PROVIDER_NAME} ({@link FrameworkMethod} method) {
 *	   return new DataProv().{@link #provide(FrameworkMethod) provide}(method);
 *	}
 *</pre>
 *</ul>
 * 
 * <p> This provider supports the following optional annotations on the same test method:
 * <ul>
 * <li> {@literal @}{@link Merge}: append data from other data provider(s) after this one.
 * The providers data is appended in the same order of appearance of the annotations.
 * <p>Also, from each merged provider, the data is first filtered and/or limited depending on the 
 * annotation specification.
 * <p>Filtering is applied before limiting i.e. the number of returned data is min(nb filtered data, limit). 
 * 
 * <li> {@literal @}{@link DataProviderFilter}: filter and/or limit the number of provided data
 * from all merged providers (including this one). 
 * <p>Filtering is applied before limiting i.e. the number of returned data is min(nb filtered data, limit). 
 * 
 * <li> {@literal @}{@link AddParams}, {@link AddIntParams} and/or {@link AddStringParams}:
 * Each data (from all merged providers) is replicated for each one of the values specified
 * by the annotation.
 * <p>Parameters from all {@link AddIntParams} are added first in the same order of appearance of the annotations,
 * then Parameters all {@link AddStringParams} are added (in same order of appearance), then Parameters 
 * from {@link AddParams} are added last (in same order of appearance).
 * <p>For example, the following test method will be provided by 4 parameterized data for each data, d, from DP:
 * [d,1,9,"a"], [d,1,9,"b"], [d,2,9,"a"], [d,2,9,"b"].
 * <p>Note the order of the test method parameters; it must be compatible with the converted data (in the same order).
 * <pre>
 * {@literal @}Test
 * {@literal @}UseDataProvider(location=DP.class)
 * {@literal @}AddIntParams({1, 2})      // param1
 * {@literal @}AddIntParams({"a", "b"})  // param2
 * {@literal @}AddIntParams(9)           // param3
 * public void testMeyhod(IData d, int param1, int param3, String param2) {
 * 	...
 * </pre>
 * </ul>
 * 
 * @author aelmouss
 */
public abstract class AbstractDataProvider<D extends IData> implements IDataProvider<D> {

	public static final boolean DEBUG = false;
	
	public static final String PROVIDER_NAME = "dataProvider";
	
	protected List<List<Object>> lastProvidedData;
	protected List<List<Object>> parameters;
	
	
	/**
	 * Return the data provided the last time this provider was used i.e.
	 * when {@link #provide(FrameworkMethod)} was last called.
	 * 
	 * @return list of provided data. Can be null.
	 */
	public List<List<Object>> getLastProvidedData() {
		return lastProvidedData;
	}
	
	/**
	 * Should try to create the data stream lazily.
	 * 
	 * @param method
	 * @return
	 */
	protected abstract Stream<D> createDataStream(FrameworkMethod method);
	
	/**
	 * This is the main method, it should be called by the DataProvider (static) method.
	 * 
	 * <p> It collects the processed data streamed by {@link #processDataStream(FrameworkMethod)}
	 * into an unmodifiable list.
	 * This list is stored in {@link #lastProvidedData} before returning it.
	 *
	 * @param method the test method
	 * 
	 * @return the list of collected data after potential Merging/filtering/limiting/adding-parameters.
	 *  Never {@code null}.
	 */
	protected List<List<Object>> provide(FrameworkMethod method) {
		Stream<D> dataStream = processDataStream(method);
		
		List<Object> data = dataStream.collect(collectingAndThen(toList(), Collections::unmodifiableList));
		
		List<List<Object>>providers = new ArrayList<>();
		providers.add(data);
		if(parameters != null && !parameters.isEmpty())
			providers.addAll(parameters);
		
		lastProvidedData = Lists.cartesianProduct(providers);
		return lastProvidedData;
	}

	/**
	 * Create the data stream by invoking {@link #createDataStream(FrameworkMethod)},
	 * then it applies the filter and/or limit - if any - specified by a 
	 * {@literal @}{@link DataProviderFilter} on the test {@code method}.
	 * @param stream 
	 * 
	 * @param method
	 * @return the filtered and/or limited data stream
	 */
	protected Stream<D> processDataStream(FrameworkMethod method) {
		Stream<D> dataStream = createDataStream(method);
		
		List<List<Object>> params = new ArrayList<>();
			
		// merge data from data providers specified with @Merge
		Stream<D> dataFromMerge = streamDataFromMerge(method);
		if(dataFromMerge != null)
			dataStream = Stream.concat(dataStream, dataFromMerge);
		
		// filter/limit using @DataProviderFilter
		DataProviderFilter filterAnnot = DataProviderUtils.getDataFilterAnnotation(method.getMethod());
		if(filterAnnot != null) {
			dataStream = filterThenLimit(dataStream, filterAnnot.filter(), filterAnnot.limit());
		}
		
		// add parameters specified with @Add*Params
		for(AddIntParams a : method.getMethod().getAnnotationsByType(AddIntParams.class)) {
			params.add(Arrays.stream(a.value()).mapToObj(Integer::new).collect(toList()));
		}
		for(AddStringParams a : method.getMethod().getAnnotationsByType(AddStringParams.class)) {
			params.add(Stream.of(a.value()).collect(toList()));
		}
		for(AddParams a : method.getMethod().getAnnotationsByType(AddParams.class)) {
			Stream<IData> paramStr = createDataStreamFromNewInstance(method, a.location());
			paramStr = filterThenLimit(paramStr, a.filter(), a.limit());
			params.add(paramStr.collect(toList()));
		}
		
		if(!params.isEmpty()) 
			this.parameters = params;
		
		return dataStream;
	}

	/**
	 * Stream data from all (if any) {@link AbstractDataProvider} data providers, specified
	 * using {@literal @}{@link Merge} annotation on the same test method.
	 * 
	 * <p> Each data provider is separately filtered/limited as specified by the associated
	 * Merge annotation.
	 * 
	 * @param method the test method
	 * 
	 * @return the merged data stream from all specified data providers, or {@code null}
	 * if none is specified.
	 */
	protected Stream<D> streamDataFromMerge(FrameworkMethod method) {
		Merge[] merges = method.getMethod().getAnnotationsByType(Merge.class);
		if(merges != null  && merges.length != 0) {
			return Arrays.stream(merges)
					.flatMap(dpInstance -> processMerge(dpInstance, method));
		}
		return null;
	}
	
	private Stream<D> processMerge(Merge merge, FrameworkMethod method) {
		Class<? extends AbstractDataProvider<? extends IData>> providerCls = merge.location();
		//TODO make sure providerCls is a provider of data types extending D !! 
		
		Stream<D> dataStream = createDataStreamFromNewInstance(method, providerCls);
		dataStream = filterThenLimit(dataStream, merge.filter(), merge.limit());
		return dataStream;
	}

	/**
	 * 
	 * @param method test method. May be {@code null}
	 * @param providerCls
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T extends IData> Stream<T> createDataStreamFromNewInstance(FrameworkMethod method,
			Class<? extends AbstractDataProvider<? extends IData>> providerCls) {
		AbstractDataProvider<? extends IData> providerInstance = DataProviderUtils.tryCreateInstace(providerCls);
		return (Stream<T>) providerInstance.createDataStream(method);
	}

	/**
	 * First apply filter (if valid) THEN limit ((if non-negative) the specified stream.
	 * 
	 * @param dataStream if {@code null} or {@link NO_FILTERING}.class no filtering is applied. 
	 * @param limit if negative no limiting is applied.
	 * @return the filtered then limited data stream.
	 */
	public static <T extends IData> Stream<T> filterThenLimit(Stream<T> dataStream, 
			Class<? extends IDataFilter<? extends IData>> filterCls, int limit) {
		dataStream = filter(dataStream, filterCls);
		
		dataStream = limit(dataStream, limit);
		
		return dataStream;
	}

	public static <T extends IData> Stream<T> filter(Stream<T> dataStream,
			Class<? extends IDataFilter<? extends IData>> filterCls) {
		if(filterCls != null && !filterCls.isAssignableFrom(DataProviderFilter.NO_FILTERING.class)) {
			if(DEBUG) System.out.println("filtering " + dataStream + " by " + filterCls);
			
			@SuppressWarnings("unchecked")
			IDataFilter<T> filter = (IDataFilter<T>) DataProviderUtils.tryCreateInstace(filterCls);
			if(filter != null) 
				dataStream = dataStream.filter(filter);
		}
		return dataStream;
	}
	
	public static <T extends IData> Stream<T> limit(Stream<T> dataStream, int limit) {
		if(limit >= 0) {
			if(DEBUG) System.out.println("limitng " + dataStream + " to " + limit);
			dataStream = dataStream.limit(limit);
		}
		return dataStream;
	}
	
//	/**
//	 * @param data the data
//	 * @return the object[][]
//	 */
//	protected IData[][] _toRawData(List<D> data) {
//		List<IData[]> filteredData = data.stream()
//			.map(d -> new IData[]{d})
//			.collect(Collectors.toList());
//		return filteredData.toArray(new IData[filteredData.size()][]);
//	}
	
}
