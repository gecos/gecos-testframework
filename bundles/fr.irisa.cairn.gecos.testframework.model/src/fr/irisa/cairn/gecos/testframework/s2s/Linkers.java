package fr.irisa.cairn.gecos.testframework.s2s;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import fr.irisa.cairn.gecos.testframework.model.IVersion;
import fr.irisa.cairn.gecos.testframework.model.TestFramework;
import fr.irisa.cairn.gecos.testframework.s2s.CompilerOperation.ICompilerFlagBuilder;

/**
 * @author aelmouss
 */
public class Linkers {
	
	private Linkers() {}
	
	
	/**
	 * Return a list with all the version's {@link IVersion#getGeneratedObjects() object files}
	 * (if not null) and all its {@link IVersion#getExternalSourceFiles() external source files}
	 * (if not null).
	 * 
	 * @param v the version.
	 * @return list of paths. Never {@code null}.
	 */
	public static final <V extends IVersion> Collection<Path> defaultLinkerSourcesProvider(V v) {
		List<Path> res = new ArrayList<>();
		List<Path> objs = v.getGeneratedObjects();
		if(objs != null) 
			res.addAll(objs);
		
		Collection<Path> ext = v.getExternalSourceFiles(); 
		if(ext != null) 
			res.addAll(ext);
		
		return res;
	}
	
	/**
	 * Return the version's {@link IVersion#getIncludeDirs() include dirs}.
	 * 
	 * @param v the version.
	 * @return list of paths.
	 */
	public static final <V extends IVersion> Collection<Path> defaultLinkerIncDirsProvider(V v) {
		return v.getIncludeDirs();
	}
	
	/**
	 * @param v the version.
	 * @return empty list
	 */
	public static final <V extends IVersion> Collection<Path> defaultLinkerLibDirsProvider(V v) {
		return Collections.emptyList();
	}
	
	/**
	 * @param v the version.
	 * @return empty list
	 */
	public static final <V extends IVersion> Collection<String> defaultLinkerLibsProvider(V v) {
		return Collections.emptyList();
	}
	
	/**
	 * @param v the version.
	 * @return empty string.
	 */
	public static final <V extends IVersion> String defaultLinkerFlagsProvider(V v) {
		return "";
	}
	

	/**
	 * Create a {@link LinkerOperation}.
	 * 
	 * @param linkerProgram the Linker shell program command.
	 * @param flagBuilder specify how the linker flags are constructed.
	 * @param sourcesProvider specify which source/object.. files of a given version to link.
	 * @param includeDirsProvider specify the include directories to use for a given source file.
	 * @param libDirsProvider specify the include directories to use for a given source file.
	 * @param libsProvider specify the libraries to use for a given source file.
	 * @param additionalFlagsProvider specify any additional flags to use for a given source file.
	 * @return a new linker operation.
	 */
	public static <V extends IVersion> LinkerOperation<V> createLinker(
			String linkerProgram, 
			Function<V, Collection<Path>> sourcesProvider,
			ICompilerFlagBuilder flagBuilder,
			Function<V, Collection<Path>> includeDirsProvider,
			Function<V, Collection<String>> libsProvider,
			Function<V, Collection<Path>> libDirsProvider,
			Function<V, String> additionalFlagsProvider) {
		return new LinkerOperation<>(linkerProgram, flagBuilder, sourcesProvider, 
				includeDirsProvider, libDirsProvider, libsProvider, additionalFlagsProvider);
	}
	
	/**
	 * Shorthand to {@link #createLinker(String, Function, ICompilerFlagBuilder, Function, Function, Function, Function)}
	 * with {@link Compilers#GccLikeFlagsBuilder} as flags builder.
	 */
	public static <V extends IVersion> LinkerOperation<V> createLinker(
			String linkerProgram, 
			Function<V, Collection<Path>> sourcesProvider,
			Function<V, Collection<Path>> includeDirsProvider,
			Function<V, Collection<String>> libsProvider,
			Function<V, Collection<Path>> libDirsProvider,
			Function<V, String> additionalFlagsProvider) {
		return new LinkerOperation<>(linkerProgram, Compilers.GccLikeFlagsBuilder, sourcesProvider, 
				includeDirsProvider, libDirsProvider, libsProvider, additionalFlagsProvider);
	}
	
	/**
	 * Create a {@link LinkerOperation} with:
	 * <ul>
	 * <li> {@link Compilers#GccLikeFlagsBuilder} as flags builder,
	 * <li> {@link #defaultLinkerIncDirsProvider(IVersion)},
	 * <li> {@link #defaultLinkerLibsProvider(IVersion)},
	 * <li> {@link #defaultLinkerLibDirsProvider(IVersion)},
	 * <li> {@link #defaultLinkerFlagsProvider(IVersion)}.
	 * </ul>
	 * <p><b>NOTE:</b> you can use {@link LinkerOperation}.change* methods to create compilers 
	 * with different providers.
	 * 
	 * @param linkerProgram the Linker shell program command.
	 * @param sourceFilesProvider specify which source files of a given version to compile.
	 * @return a linker operation.
	 */
	public static final <V extends IVersion> LinkerOperation<V> createLinker(String linkerProgram,
			Function<V, Collection<Path>> sourceFilesProvider) {
		return Linkers.<V>createLinker(linkerProgram, sourceFilesProvider, Compilers.GccLikeFlagsBuilder, 
				Linkers::defaultLinkerIncDirsProvider,
				Linkers::defaultLinkerLibsProvider,
				Linkers::defaultLinkerLibDirsProvider,
				Linkers::defaultLinkerFlagsProvider);
	}
	
	/**
	 * Create a {@link LinkerOperation} with:
	 * <ul>
	 * <li> {@link Compilers#GccLikeFlagsBuilder} as flags builder,
	 * <li> {@link #defaultLinkerSourcesProvider(IVersion)},
	 * <li> {@link #defaultLinkerIncDirsProvider(IVersion)},
	 * <li> {@link #defaultLinkerLibsProvider(IVersion)},
	 * <li> {@link #defaultLinkerLibDirsProvider(IVersion)},
	 * <li> {@link #defaultLinkerFlagsProvider(IVersion)}.
	 * </ul>
	 * <p><b>NOTE:</b> you can use {@link LinkerOperation}.change* methods to create compilers 
	 * with different providers.
	 * 
	 * @param linkerProgram the Linker shell program command.
	 * @return a linker operation.
	 */
	public static final <V extends IVersion> LinkerOperation<V> createLinker(String linkerProgram) {
		return Linkers.<V>createLinker(linkerProgram, 
				Linkers::defaultLinkerSourcesProvider,
				Compilers.GccLikeFlagsBuilder, 
				Linkers::defaultLinkerIncDirsProvider,
				Linkers::defaultLinkerLibsProvider,
				Linkers::defaultLinkerLibDirsProvider,
				Linkers::defaultLinkerFlagsProvider);
	}
	
	/**
	 * Shorthand for {@link #createLinker(String)}
	 * with "cc" as linker program.
	 * 
	 * @return a linker operation.
	 */
	public static final <V extends IVersion> LinkerOperation<V> cc() {
		return createLinker(TestFramework.findProgram("cc"));
	}
	
	/**
	 * Shorthand for {@link #createLinker(String)}
	 * with "gcc" as linker program.
	 * 
	 * @return a linker operation.
	 */
	public static final <V extends IVersion> LinkerOperation<V> gcc() {
		return createLinker(TestFramework.findProgram("gcc"));
	}
	
	/**
	 * Shorthand for {@link #createLinker(String)}
	 * with "gcc" as linker program and "-std=c99 -pedantic" as 
	 * additional flags.
	 * 
	 * @return a linker operation.
	 */
	public static final <V extends IVersion> LinkerOperation<V> gccC99() {
		return Linkers.<V>createLinker(TestFramework.findProgram("gcc"))
				.changeFlagsProvider(v -> "-std=c99 -pedantic");
	}
	
	/**
	 * Shorthand for {@link #createLinker(String)}
	 * with "clang" as linker program.
	 * 
	 * @return a linker operation.
	 */
	public static final <V extends IVersion> LinkerOperation<V> clang() {
		return createLinker(TestFramework.findProgram("clang"));
	}
	
	/**
	 * Shorthand for {@link #createLinker(String)}
	 * with "mpicc" as linker program.
	 * 
	 * @return a linker operation.
	 */
	public static final <V extends IVersion> LinkerOperation<V> mpicc() {
		return createLinker(TestFramework.findProgram("mpicc"));
	}
}
