package fr.irisa.cairn.gecos.testframework.data;

import java.nio.file.Path;
import java.util.List;

/**
 * Represent a Data from a single source file.
 * 
 * @author aelmouss
 */
public abstract class AbstractCxxData extends AbstractDataFromPath implements ICxxProjectData {
	
	protected List<Path> srcFiles;
	protected List<Path> incDirs;
	protected Path makefile;
	
	@Override
	public List<Path> getSourceFiles() {
		return srcFiles;
	}
	
	@Override
	public List<Path> getIncludeDirs() {
		return incDirs;
	}
	
	@Override
	public Path getMakefile() {
		return makefile;
	}

	@Override
	public void setMakefile(Path makefile) {
		this.makefile = makefile;
	}
	
	
//	/**
//	 * Checks for makefile.
//	 *
//	 * @return true, if successful
//	 */
//	public boolean hasMakefile() {
//		return makefile != null;
//	}
	
}
