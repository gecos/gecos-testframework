//package fr.irisa.cairn.gecos.testframework.model;
//
//import java.util.List;
//
//import fr.irisa.cairn.gecos.testframework.exceptions.OperationFailureException;
//
//public interface IDataToVersions<D extends IData, V extends IVersion> {
//	
//	public List<V> createVersions(D data, List<IVersionOperator<V>> transforms) throws OperationFailureException;
//	
//}
