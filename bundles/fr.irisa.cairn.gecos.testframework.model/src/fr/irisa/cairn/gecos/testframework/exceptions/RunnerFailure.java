package fr.irisa.cairn.gecos.testframework.exceptions;

public class RunnerFailure extends OperationFailureException {

	/**
	 * Instantiates a new executor failure exception.
	 *
	 * @param msg the msg
	 */
	public RunnerFailure(String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new executor failure exception.
	 *
	 * @param string the string
	 * @param e the e
	 */
	public RunnerFailure(String string, Exception e) {
		super(string, e);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8740512042700764649L;

}
