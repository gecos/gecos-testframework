package fr.irisa.cairn.gecos.testframework.s2s;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

import fr.irisa.cairn.gecos.testframework.exceptions.ComparatorFailure;
import fr.irisa.cairn.gecos.testframework.model.IVersion;
import fr.irisa.cairn.gecos.testframework.model.IVersionBiOperator;
import fr.irisa.r2d2.gecos.framework.utils.FileUtils;

/**
 * 
 * @author aelmouss
 */
public class Comparators {

	private Comparators() {}
	
	/**
	 * @return a {@link IVersionBiOperator} for comparing 2 versions based on their 
	 * execution {@link IVersion#getExitCode() exit code}.
	 * @throws ComparatorFailure if exit codes differ.
	 */
	public static <V extends IVersion> IVersionBiOperator<V> exitCodeEquals() {
		return (v1, v2) -> {
			if(! v1.getExitCode().equals(v2.getExitCode()))
				throw new ComparatorFailure("[COMPARATOR]: the exit codes of versions " + v1 + " and " + v2 + " are different!");
		};
	}
	
	/**
	 * Create a {@link IVersionBiOperator} for comparing 2 versions based on one file 
	 * or directory associated with each.
	 * 
	 * <p> The comparator first determine the file associated with each version.
	 * This is done by applying {@code fileProvider} to it.
	 * 
	 * <p> Then, each file is checked by the {@code fileChecker} to it.
	 * A {@link ComparatorFailure} is thrown in case the checker returns false.
	 * 
	 * <p> Both files are then compared by the {@code filePairComparator}.
	 * A {@link ComparatorFailure} is thrown in case the comparator returns false.
	 * 
	 * @param fileProvider determine the file associated with the specified version.
	 * @param fileChecker test if the specified file is valid; return false if not valid.
	 * @param filePairComparator compare the file pair; return false if comparison fails.
	 * 
	 * @return a {@link IVersionBiOperator} for comparing 2 versions based on one file/dir associated with each.
	 * @throws ComparatorFailure if comparison fails
	 */
	public static <V extends IVersion> IVersionBiOperator<V> fileComparator(
			Function<V, Path> fileProvider,
			Predicate<Path> fileChecker,
			BiPredicate<Path, Path> filePairComparator
			) {
		return (V version, V toVersion) -> {
			Path outputRef = fileProvider.apply(toVersion);
			Path output = fileProvider.apply(version);
			
			if(!fileChecker.test(output))
				throw new ComparatorFailure("[COMPARATOR]: the file from version " + version + " was not found! Looked at " + output);
			if(!fileChecker.test(outputRef))
				throw new ComparatorFailure("[COMPARATOR]: the file from version " + toVersion + " was not found! Looked at " + outputRef);
				
			if(!filePairComparator.test(output, outputRef))
				throw new ComparatorFailure("[COMPARATOR]: the files of versions " + version + " and " + toVersion + " are different!");
		};
	}
	
	/**
	 * Shortcut for {@link #fileComparator(Function, Predicate, BiPredicate)} with:
	 * <li> file-exist-and-is-a-regular-file as file checker. 
	 * 
	 * @param fileProvider determine the file associated with the specified version.
	 * @param filePairComparator compare the file pair; return false if comparison fails.
	 * 
	 * @return a {@link IVersionBiOperator} for comparing 2 versions based on one file associated with each.
	 */
	public static <V extends IVersion> IVersionBiOperator<V> regularFileComparator(Function<V, Path> fileProvider, 
			BiPredicate<Path, Path> filePairComparator) {
		return fileComparator(fileProvider, Files::isRegularFile, filePairComparator);
	}
	
	/**
	 * Shortcut for {@link #regularFileComparator(Function, BiPredicate)} with:
	 * <li> byte-to-byte file comparator.
	 * 
	 * @param fileProvider determine the file associated with the specified version.
	 * 
	 * @return a {@link IVersionBiOperator} for comparing 2 versions based on one file associated with each.
	 */
	public static <V extends IVersion> IVersionBiOperator<V> fileEquals(Function<V, Path> fileProvider) {
		return regularFileComparator(fileProvider, (f, fRef) -> FileUtils.areFilesEquals(f.toFile(), fRef.toFile()));
	}
	
	/**
	 * Shortcut for {@link #regularFileComparator(Function, BiPredicate)} with:
	 * <li> {@link IVersion#getStdoutFile()} as file provider.
	 * 
	 * @param filePairComparator compare the file pair; return false if comparison fails.
	 * 
	 * @return a {@link IVersionBiOperator} for comparing 2 versions based on their stdout files.
	 */
	public static <V extends IVersion> IVersionBiOperator<V> stdoutComparator(BiPredicate<Path, Path> filePairComparator) {
		return regularFileComparator(IVersion::getStdoutFile, filePairComparator);
	}
	
	/**
	 * Shortcut for {@link #regularFileComparator(Function, BiPredicate)} with:
	 * <li> {@link IVersion#getStderrFile()} as file provider.
	 * 
	 * @param filePairComparator compare the file pair; return false if comparison fails.
	 * 
	 * @return a {@link IVersionBiOperator} for comparing 2 versions based on their stderr files.
	 */
	public static <V extends IVersion> IVersionBiOperator<V> stderrComparator(BiPredicate<Path, Path> filePairComparator) {
		return regularFileComparator(IVersion::getStderrFile, filePairComparator);
	}
	
	/**
	 * Shortcut for {@link #fileEquals(Function)} with:
	 * <li> {@link IVersion#getStdoutFile()} as file provider.
	 * 
	 * <p><b>NOTE:</b> The versions stdout file must be set and generated (with an executor for instance). 
	 * 
	 * @return a {@link IVersionBiOperator} for comparing 2 versions based on their stdout files.
	 */
	public static <V extends IVersion> IVersionBiOperator<V> stdoutEquals() {
		return fileEquals(IVersion::getStdoutFile);
	}
	
	/**
	 * Shortcut for {@link #fileEquals(Function)} with:
	 * <li> {@link IVersion#getStderrFile()} as file provider.
	 * 
	 * <p><b>NOTE:</b> The versions stderr file must be set and generated (with an executor for instance). 
	 * 
	 * @return a {@link IVersionBiOperator} for comparing 2 versions based on their stderr files.
	 */
	public static <V extends IVersion> IVersionBiOperator<V> stderrEquals() {
		return fileEquals(IVersion::getStderrFile);
	}
	
	/**
	 * Create a {@link IVersionBiOperator} for comparing the source codes files of 2 versions.
	 */
	public static <V extends IVersion> IVersionBiOperator<V> sourceCodeComparator(
			BiPredicate<Path, Path> filePairComparator) {
		return (v1, v2) -> v1.getGeneratedSourceFiles().forEach(f1 -> {
			Path f2 = findMatchingSource(v2, f1);
			if(!filePairComparator.test(f1, f2))
				throw new ComparatorFailure("[COMPARATOR]: the source code files " + f1 + " and " 
						+ f2 + " of versions " + v1 + " and " + v2 + " are different!");
		});
	}
	
	public static <V extends IVersion> IVersionBiOperator<V> sourceCodeEquals() {
		return sourceCodeComparator((f, fRef) -> FileUtils.areFilesEquals(f.toFile(), fRef.toFile()));
	}
	
	private static <V extends IVersion> Path findMatchingSource(V version, Path file) {
		return version.getGeneratedSourceFiles().stream()
			.filter(f1 -> f1.getFileName().equals(file.getFileName()))
			.findFirst().orElseThrow(() -> new ComparatorFailure(
				"[COMPARATOR] could not find matching source file for " + file + " in version " + version));
	}

}
