package fr.irisa.cairn.gecos.testframework.utils;

import static java.util.stream.Collectors.joining;

import java.nio.file.Path;
import java.util.Collection;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import fr.irisa.cairn.gecos.testframework.model.TestFramework;
import fr.irisa.r2d2.gecos.framework.utils.TimeoutException;

public final class OperationUtils {

	private OperationUtils() { }
	
	public static final <T> String join(Collection<T> collection, Function<T, String> element) {
		return join(collection, element, " ");
	}
	
	public static final <T> String join(Collection<T> collection, Function<T, String> element, String separator) {
		return collection.stream()
				.map(element)
				.collect(joining(separator));
	}
	
	public static final <T> Collection<T> filter(Collection<T> collection, Predicate<T> filter) {
		return collection.stream().filter(filter).collect(Collectors.toList());
	}
	
	/**
	 * Executes the specified {@code cmd} using the default shell in {@link TestFramework}.
	 *
	 * @param cmd the cmd
	 * @param timeout the timeout
	 * @param stdout if not {@code null}, stdout is redirected to this file.
	 * @param stderr if not {@code null}, stderr is redirected to this file.
	 * @return exit code of the {@code cmd}
	 * @throws RuntimeException if fail or timeout.
	 */
	public static int executeCmd(String cmd, Long timeout, Path stdout, Path stderr) {
		int res; try {
			res = TestFramework.invoke(cmd, timeout, stdout, stderr);
		} catch(TimeoutException e) {
			throw new RuntimeException("Command timeout: " + cmd, e);
		}
		if(res != TestFramework.shell.successExitCode())
			throw new RuntimeException("Command execution failed: " + cmd);
		return res;
	}
	
	/**
	 * Shorthand to {@link #executeCmd(String, Long, Path, Path)} with null stdout/stderr.
	 */
	public static int executeCmd(String cmd, Long timeout) {
		return executeCmd(cmd, timeout, null, null);
	}
}
