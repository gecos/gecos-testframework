package fr.irisa.cairn.gecos.testframework.dataprovider;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import fr.irisa.cairn.gecos.testframework.model.IData;
import fr.irisa.cairn.gecos.testframework.model.IDataFilter;

/**
 * Used by {@link AbstractDataProvider} to filter and/or its provided data.
 * 
 * <p> Defaults to no filtering and no limiting.
 * 
 * <p><b>NOTE:</b>
 * <li> The specified filter class, in case it is a nested class, should be static and publicly accessible !
 * <li> The filter shouldn't filter out ALL data i.e. the filtered/limited data set should not be empty.
 * 
 * @author aelmouss
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DataProviderFilter {
	
	public class NO_FILTERING implements IDataFilter<IData> {
		public boolean test(IData t) {
			return true;
		}
	}
	
	/**
	 * Can be used to filter the data provided by a {@link AbstractDataProvider}.
	 * 
	 * <p> Defaults to {@link NO_FILTERING} i.e. no filtering.
	 *
	 * @return the class<? extends I data filter<? extends I data>>
	 */
	Class<? extends IDataFilter<? extends IData>> filter() default NO_FILTERING.class;

	/**
	 * Optional maximum number of data to be provided by the specified provider.
	 * Only works with {@link AbstractDataProvider} providers.
	 * 
	 * <p> No limit is applied if negative.
	 * 
	 * <p> Defaults to -1 (i.e. no limit).
	 * 
	 * @return the maximum number of data to provide.
	 */
	int limit() default -1;
	
}
