package fr.irisa.cairn.gecos.testframework.dataprovider;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Used by {@link AbstractDataProvider} to append one integer parameter 
 * to each provided data. The data is replicated for each of the
 * specified integer values.
 * 
 * @author aelmouss
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Repeatable(AddIntParamsList.class)
public @interface AddIntParams {

	/**
	 * @return array of values for the integer parameter to be added.
	 */
	int[] value();
	
}

