package fr.irisa.cairn.gecos.testframework.s2s;

import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Objects;

import com.google.common.collect.LinkedHashMultimap;

import fr.irisa.cairn.gecos.testframework.model.ITestStage;
import fr.irisa.cairn.gecos.testframework.s2s.TestFlow.IStageName;

public class TestFlowBuilder {
	
	protected IStageName[] names;
	protected LinkedHashMap<IStageName, ITestStage> namedStages;
	protected LinkedHashMultimap<IStageName, ITestStage> thenStages;
	protected IStageName lastNamedStage;
	
	protected TestFlowBuilder(IStageName[] names) {
		if(names == null || names.length == 0)
			throw new InvalidParameterException("names cannot be null or empty!");
		this.names = names;
		this.lastNamedStage = names[0];
		this.namedStages = new LinkedHashMap<>();
		this.thenStages = LinkedHashMultimap.create();
	}

	public TestFlow build() {
		TestFlow flow = new TestFlow();
		flow.names = names;
		flow.namedStages = namedStages;
		flow.thenStages = thenStages;
		return flow;
	}
	
	protected void validName(IStageName name) {
		if(!Arrays.stream(names).anyMatch(name::equals))
			throw new InvalidParameterException("The specified stage name '" + name +
					"' is not valid. Must be one of :" + names);
	}
	
	/**
	 * Set the value of the named stage.
	 * 
	 * @param name the stage name
	 * @param stage the stage value
	 * @return this
	 */
	public TestFlowBuilder named(IStageName name, ITestStage stage) {
		Objects.requireNonNull(stage);
		validName(name);
		lastNamedStage = name;
		namedStages.put(name, stage);
		return this;
	}
	
	/**
	 * Add an unnamed stage in order after the last added named stage.
	 * 
	 * @param stage unnamed stage.
	 * @return this
	 */
	public TestFlowBuilder then(ITestStage stage) {
		Objects.requireNonNull(stage);
		thenStages.put(lastNamedStage, stage);
		return this;
	}
}