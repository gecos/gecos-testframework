package fr.irisa.cairn.gecos.testframework.stages;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import fr.irisa.cairn.gecos.testframework.model.ITestStage;

public class SequenceStage implements ITestStage {
	
    private ITestStage[] stagesFIFO; // first stage is applied first

    public SequenceStage(ITestStage... rules) {
        this.stagesFIFO = rules;
    }

    public List<ITestStage> findStagesByTag(String tag) {
		return Arrays.stream(stagesFIFO)
			.filter(s -> s.getTag().equals(tag))
			.collect(Collectors.toList());
	}

    @Override
    public void apply(Object target) {
        for (ITestStage each : stagesFIFO)
            each.apply(target);
    }
    
}