package fr.irisa.cairn.gecos.testframework.dataprovider;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.runners.model.FrameworkMethod;

import com.google.common.base.Strings;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.testframework.data.AbstractDataFromPath;
import fr.irisa.cairn.gecos.testframework.utils.DataProviderUtils;
import fr.irisa.cairn.gecos.testframework.utils.ResourceLocator;

/**
 * This implements a {@link AbstractDataProvider} intended for use within the 
 * {@literal @}{@link UseDataProvider} or {@literal @} {@link Merge} 
 * annotations, annotating a test method.
 * 
 * <p> It provide {@link AbstractDataFromPath} instances from compatible
 * files within one or more resources location.
 * 
 * <p> The resource locations must be provided using one or more 
 * {@literal @}{@link ResourcesLocation} annotations on the same test method.
 * 
 * @author aelmouss
 */
public class DataFromPathProvider extends AbstractDataProvider<AbstractDataFromPath>  {
	
	@DataProvider 
	public static List<List<Object>> dataProvider(FrameworkMethod method) {
		return new DataFromPathProvider().provide(method);
	}
	
	@Override
	protected Stream<AbstractDataFromPath> createDataStream(FrameworkMethod method) {
		ResourcesLocation[] locations = method.getMethod().getAnnotationsByType(ResourcesLocation.class);
		return streamDataFromLocations(method, Arrays.asList(locations));
	}
	

	public static Stream<AbstractDataFromPath> streamDataFromLocations(FrameworkMethod method, 
			Collection<ResourcesLocation> locations) {
		if(locations.isEmpty())
			throw new IllegalArgumentException("The test method '" + method + "', using data provider '" 
					+ DataFromPathProvider.class + "', must be also annotated with one or more @" 
					+ ResourcesLocation.class);
		
		return locations.stream()
				.flatMap(l -> streamDataFormLocation(l, method));
	}
	
	//TODO create stream lazily for more efficient filtering/limiting
	public static Stream<AbstractDataFromPath> streamDataFormLocation(ResourcesLocation l, FrameworkMethod method) {
		if(l.dataClasses().length == 0)
			throw new IllegalArgumentException("ERROR: No DataFactory is specified for the ResourcesLocation: " + l);
		
		String bundleName = l.bundleName();
		String resourcesLocation = l.value();
		ResourceLocator locator;
		if(Strings.isNullOrEmpty(bundleName)) {
			Class<?> methodClass = method.getMethod().getDeclaringClass();
			locator = new ResourceLocator(methodClass , resourcesLocation);
		} else {
			locator = new ResourceLocator(bundleName, resourcesLocation);
		}
		locator.setReuseExtractedJars(true);
		Path rootDir = locator.locate(resourcesLocation);
		
		if(!Files.isDirectory(rootDir)) {
			throw new RuntimeException("ERROR: resources resolved loaction '" + rootDir 
					+ "' not found! Specified by " + l);
		}
		
		final List<Class<? extends AbstractDataFromPath>> factories = Arrays.stream(l.dataClasses())
				.collect(Collectors.toCollection(ArrayList::new));
		
		try {
			return DataProviderUtils.createCompatibleData(rootDir, Integer.MAX_VALUE, factories, l.limit()).stream();
		} catch (Exception e) {
			throw new RuntimeException("ERROR: failed to create data from ResourcesLocation: " + l, e);
		}
	}

}
