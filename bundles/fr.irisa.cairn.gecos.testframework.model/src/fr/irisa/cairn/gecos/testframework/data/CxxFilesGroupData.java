package fr.irisa.cairn.gecos.testframework.data;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;

import fr.irisa.cairn.gecos.testframework.exceptions.InvalidDataException;
import fr.irisa.cairn.gecos.testframework.utils.DataProviderUtils;

/**
 * Represent a directory that contains a group of Cxx source files.
 * The directory name must end with <b>'.group'</b>.
 * 
 * <p> All source files (whose name end with '.c', '.cpp' or '.cc') in the 
 * group directory (infinite depth) are added to the list of sources.
 * The group directory must have at least one such source file.
 * <p> All directories that contain at least one header file (whose name end 
 * with '.h', 'hpp' or '.hh') in the group directory (infinite depth) are 
 * added to the list of include dirs.
 * <p> It may optionally contain a makefile (named 'makefile' or 'Makefile')
 * directly under the group directory (depth 1).
 * 
 * @author aelmouss
 */
public class CxxFilesGroupData extends AbstractCxxData {
	
	public static boolean isValidPath(Path dir) {
		return dir != null && Files.isDirectory(dir) 
				&& dir.getFileName().toString().matches(FileNamePatterns.CXX_GROUP_DIR);
	}
	
	public static CxxFilesGroupData createFromPath(Path dir) throws InvalidDataException {
		if(!isValidPath(dir))
			throw new InvalidDataException("Not a valid '" + CxxFileData.class.getSimpleName() + "' data: " + dir);
	
		CxxFilesGroupData data = new CxxFilesGroupData();
		data.path = dir;
		data.srcFiles = DataProviderUtils.lookupFiles(dir, FileNamePatterns.CXX_SRC).collect(Collectors.toList());
		if(data.srcFiles.isEmpty())
			throw new InvalidDataException("Group directory of '" + CxxFileData.class.getSimpleName() 
					+ "' data must contain at least one source file: " + dir);
		data.incDirs = DataProviderUtils.lookupDirsContainingFile(dir, FileNamePatterns.HEADERFILE_NAME_PATTERN)
				.collect(Collectors.toList());
		data.makefile = DataProviderUtils.lookupFiles(dir, 1, FileNamePatterns.MAKEFILE).findFirst().orElse(null);
		return data;
	}
	
}
