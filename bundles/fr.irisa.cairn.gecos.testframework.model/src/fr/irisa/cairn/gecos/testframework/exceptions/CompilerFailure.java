package fr.irisa.cairn.gecos.testframework.exceptions;

public class CompilerFailure extends OperationFailureException {

	/**
	 * Instantiates a new compiler failure exception.
	 *
	 * @param msg the msg
	 */
	public CompilerFailure(String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new compiler failure exception.
	 *
	 * @param string the string
	 * @param e the e
	 */
	public CompilerFailure(String string, Exception e) {
		super(string, e);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8740512042700764649L;

}
