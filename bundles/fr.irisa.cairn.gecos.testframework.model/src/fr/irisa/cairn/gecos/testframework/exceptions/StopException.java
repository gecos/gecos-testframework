package fr.irisa.cairn.gecos.testframework.exceptions;

public class StopException extends RuntimeException {

	private static final long serialVersionUID = 5197028161069012300L;

	public StopException(String msg) {
		super(msg);
	}

	public StopException(String string, Exception e) {
		super(string, e);
	}

}
