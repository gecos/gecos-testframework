package fr.irisa.cairn.gecos.testframework.exceptions;

public class LinkerFailure extends OperationFailureException {

	/**
	 * Instantiates a new compiler failure exception.
	 *
	 * @param msg the msg
	 */
	public LinkerFailure(String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new compiler failure exception.
	 *
	 * @param string the string
	 * @param e the e
	 */
	public LinkerFailure(String string, Exception e) {
		super(string, e);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8740512042700764649L;

}
