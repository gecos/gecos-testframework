package fr.irisa.cairn.gecos.testframework.stages;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;

import fr.irisa.cairn.gecos.testframework.exceptions.OperationFailureException;
import fr.irisa.cairn.gecos.testframework.exceptions.CreationFailure;
import fr.irisa.cairn.gecos.testframework.model.AbstractTestTemplate;
import fr.irisa.cairn.gecos.testframework.model.ITestStage;
import fr.irisa.cairn.gecos.testframework.model.IVersion;
import fr.irisa.cairn.gecos.testframework.model.IVersionOperator;

/**
 * Create the transformed versions by applying the testtemplate transformations
 * on the original version (v0).
 * 
 * <p> Depending of the value of {@link #sequential}, the transformations
 * are applied either:
 * <ul>
 * <li> Sequentially; each transformation (Ti) is applied on a copy of the 
 * result of the previous one, starting from v0: <i> vi+1 = Ti(copy(vi)) </i>
 * <li> Or, on a copy of v0: <i> vi+1 = Ti(copy(<b>v0</b>)) </i>
 * </ul>
 * 
 * <p> All created versions are added, in original-first order, to the
 * versions list of the test template. 
 * 
 * @author aelmouss
 */
public class TransformationStage<V extends IVersion> implements ITestStage {
	
	protected final boolean sequential;
	protected Function<V, V> copier;
	
	public TransformationStage(Function<V, V> versionCopier, boolean sequential) {
		Objects.requireNonNull(versionCopier);
		this.copier = versionCopier;
		this.sequential = sequential;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void apply(Object target) {
		AbstractTestTemplate<V> testTemplate = (AbstractTestTemplate<V>)target;
		List<IVersionOperator<V>> transforms = testTemplate.getTransforms();
		List<V> versions = testTemplate.getVersions();
		
		V original = null;
		if(!versions.isEmpty())
			original = versions.get(0);
		if(original == null)
			new RuntimeException("No original data is found in: " + target + " ! A ConvertionStage "
					+ "should be used prior to this in order to create the original version.");
		
		V previous = original;
		for (int i = 0; i < transforms.size(); i++) {
			try {
				V transVer = copier.apply(previous);
				transVer.setPrevious(previous);
				transVer.setName("Trans_" + i);
				
				transforms.get(i).apply(transVer);
				versions.add(transVer);
				
				previous = sequential? transVer : original;
			} catch (OperationFailureException e) {
				throw new CreationFailure("Failed creating transformed version: " + i, e); //XXX
			}
		}
	}
	
 }