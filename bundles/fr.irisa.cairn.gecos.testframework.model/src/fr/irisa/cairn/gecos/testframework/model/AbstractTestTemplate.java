package fr.irisa.cairn.gecos.testframework.model;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.AssumptionViolatedException;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import fr.irisa.cairn.gecos.testframework.dataprovider.AbstractDataProvider;
import fr.irisa.cairn.gecos.testframework.dataprovider.DataFromPathProvider;
import fr.irisa.cairn.gecos.testframework.exceptions.OperationFailureException;
import fr.irisa.cairn.gecos.testframework.exceptions.StopException;
import fr.irisa.cairn.gecos.testframework.utils.ExceptionsHandler;
import fr.irisa.r2d2.gecos.framework.utils.FileUtils;

/**
 * This test template is run using {@link DataProviderRunner}.
 * 
 * <p> A test class implementation can contain one or more test methods
 * annotated with {@literal@}{@link Test} and with 
 * {@literal@}{@link UseDataProvider} that specifies an 
 * {@link AbstractDataProvider} in order to provide data inputs
 * for the test method.
 * 
 * <p> Each test method MUST run the test by invoking 
 * {@link #runTest(IData, IVersionOperator...)}.
 * 
 * <p> Note that a new instance of this class is created for each 
 * provided {@link IData} for each test method.
 * 
 * @see UseDataProvider
 * @see DataFromPathProvider
 * 
 * @author aelmouss
 */
@RunWith(DataProviderRunner.class)
public abstract class AbstractTestTemplate<V extends IVersion> {

	private static final String DEFAULT_OUTPUT_DIR = "./target/test-regen/";

	// Configurations
	protected ExceptionsHandler expectedExceptions;
	protected ExceptionsHandler skipOnExceptions;
	
	protected Path testMethodOutputDir;
	protected boolean deleteMethodOutputDirOnExit;
	protected boolean cleanMethodOutputDirBefore;
	
	// test template data: used by stages 
	protected IData data;
	protected List<IVersionOperator<V>> transforms;
	protected List<V> versions;
	
	
	protected AbstractTestTemplate() {
		deleteMethodOutputDirOnExit = false;
		cleanMethodOutputDirBefore = true;
		expectedExceptions = ExceptionsHandler.none();
		skipOnExceptions = ExceptionsHandler.none();
		
		beforeTestMethod();
	}

	public List<V> getVersions() {
		return versions;
	}
	
	public void setVersions(List<V> versions) {
		this.versions = versions;
	}
	
	public IData getData() {
		return data;
	}
	
	public List<IVersionOperator<V>> getTransforms() {
		return transforms;
	}
	
	public Path getTestMethodOutputDir() {
		return testMethodOutputDir;
	}
	
	/**
	 * This method is invoked before the test method.
	 * So anything it does is visible by all test methods and
	 * can be overwritten by any/all of them.
	 */
	protected void beforeTestMethod() { }
	
	
	/**
	 * This is the entry point of the test and should be called by each test method.
	 * It does the following:
	 * <ul>
	 * <li> Set the current test data and transforms, and create a new empty list of versions.
	 * Also set default values for {@link #testMethodOutputDir}.
	 * <li> Invoke {@link #beforeBuildTestFlow()}.
	 * <li> Build the test flow by invoking {@link #buildTestFlow()}.
	 * <li> Then run it by invoking {@link #runTestFlow(ITestStage)}.
	 * </ul>
	 * @param data input data.
	 * @param transforms transformations to create different versions from the input data. 
	 */
	@SafeVarargs
	public final void runTest(IData data, final IVersionOperator<V>... transforms) {
		this.data = data;
		this.transforms = Arrays.asList(transforms);
		this.versions = new ArrayList<>();
		this.testMethodOutputDir = createTestMethodOutputDir(DEFAULT_OUTPUT_DIR, 
				getClass().getSimpleName(), Thread.currentThread().getStackTrace()[2], data.getName());
		
		beforeBuildTestFlow();
		
		// build test flow
		ITestStage testFlow = buildTestFlow();
		
		// run test flow
		runTestFlow(testFlow);
		
	}
	
	/**
	 * Invoked by {@link #runTest(IData, IVersionOperator...)} before {@link #buildTestFlow()}.
	 */
	protected void beforeBuildTestFlow() { }
	
	protected abstract ITestStage buildTestFlow();
	
	protected void runTestFlow(ITestStage testFlow) {
		boolean caughtExpected = false;
		boolean caughtStop = false;
		
		try {
			testFlow.apply(this);
		} catch(StopException e) {
			caughtStop = true;
		} catch (Exception e) {
			// check if expected
			if(expectedExceptions.matches(e)) {
				caughtExpected = true;
				success("Ending test with success after encountering the registered 'expected' exception: " + e.getClass().getSimpleName(), e);
			}
			else if(expectedExceptions.isAnyExceptionRegistered())
				fail("Failed because exception '" + e.getClass().getSimpleName() + "' is caught but was expecting: " + expectedExceptions, e);
			
			// check if skipOn
			else if(skipOnExceptions.matches(e))
				skip("Skipping test after encountering the registered 'skipOn' exception: " + e.getClass().getSimpleName(), e);
			
			// check if failure
			else if(e instanceof OperationFailureException)
				fail("", e);
			
			// any other case is considered as an unexpected error
			else
				error(e);
		} finally {
			afterRunTestFlow();
			
			if(deleteMethodOutputDirOnExit)
				FileUtils.deleteRecursive(testMethodOutputDir.toFile());
		}
		
		if(!caughtExpected && expectedExceptions.isAnyExceptionRegistered())
			fail("Failed because no exception was thrown but was expecting: " + expectedExceptions, null);
		else if(caughtStop)
			success("Ending test with success after encountering a StopException", null);
	}

	
	/**
	 * Invoked by {@link #runTestFlow(ITestStage)} after the test flow is applied
	 * in the finally block.
	 */
	protected void afterRunTestFlow() { }
	

	/**
	 * End test with success status.
	 * 
	 * @param string
	 * @param e
	 */
	private void success(String string, Throwable e) {
		System.out.println(string + " " + e);
		
	}

	/**
	 * End test with Skpipped state.
	 *
	 * @param msg the msg
	 * @param e the e
	 * @throws AssumptionViolatedException the assumption violated exception
	 */
	private void skip(String msg, Throwable e) {
		throw new AssumptionViolatedException("[SKIP] " + msg, e);
	}
	
	/**
	 * End test with Failed state.
	 *
	 * @param msg the msg
	 * @param e the e
	 * @throws AssertionError the assertion error
	 */
	private void fail(String msg, Throwable e) throws AssertionError {
		throw new AssertionError("[FAIL] " + msg + getAllMessages(e), e);
	}
	
	/**
	 * End test with Error state.
	 * @param e
	 */
	private void error(Exception e) {
		throw new RuntimeException("[ERROR] " + getAllMessages(e), e);
	}
	
	/**
	 * Gets the all messages.
	 *
	 * @param e the e
	 * @return the all messages
	 */
	private static String getAllMessages(Throwable e) {
		StringBuilder message = new StringBuilder();
		Throwable cause = e;
		while(cause != null) {
			message.append("\n|_ Caused by ")
				.append(cause.getClass().getSimpleName())
				.append(": ").append(cause.getMessage());
			cause = cause.getCause();
		}
		return message.toString();
	}
	
	
	
	private int methodIdx = 0;
	private int dataIdx = 0;
	private Path createTestMethodOutputDir(String rootDir, String testClassName, 
			StackTraceElement testMethodStackTrace, String dataName) {
		try {
			Path dir = Paths.get(rootDir).resolve(testClassName);
				
			String testMethodName;
			try {
				testMethodName = testMethodStackTrace.getMethodName();
			} catch (Exception e) {
				testMethodName = "testMethod_" + (methodIdx++);
			}
			dir = dir.resolve(testMethodName);
			
			try {
				String[] tmp = dataName.split("/");
				dataName = tmp[tmp.length-1].replaceAll(File.separator, "_").replaceAll(" ", "-");
			} catch (Exception e) {
				dataName = "data_" + (dataIdx++);
			}
			
			Path rootFolder = dir.resolve(dataName);
			if(cleanMethodOutputDirBefore && Files.exists(rootFolder))
				FileUtils.deleteRecursive(rootFolder.toFile());
			
			// Files.createDirectories(rootFolder);
			
			return rootFolder;
		} catch (Exception e) {
			throw new RuntimeException(); //XXX
		}
	}
	
}
