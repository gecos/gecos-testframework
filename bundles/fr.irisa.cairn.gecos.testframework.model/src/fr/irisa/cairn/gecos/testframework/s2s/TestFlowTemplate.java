package fr.irisa.cairn.gecos.testframework.s2s;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import fr.irisa.cairn.gecos.testframework.model.AbstractTestTemplate;
import fr.irisa.cairn.gecos.testframework.model.IData;
import fr.irisa.cairn.gecos.testframework.model.ITestStage;
import fr.irisa.cairn.gecos.testframework.model.IVersion;
import fr.irisa.cairn.gecos.testframework.stages.Stages;

/**
 * An implementation of {@link AbstractTestTemplate} that allows to dynamically
 * register (and modify) source-to-source test flow for different {@link IData} 
 * classes.
 *
 * <p> It keeps a map of {@link TestFlow} for a {@link IData} class.
 * These test flows can be registered/modified:<ul>
 * <li> by overriding {@link #configure()}: invoked before a test method.
 * This makes it common to all test methods.
 * <li> in a test method before starting the test (i.e. before invoking 
 * {@link #runTest(IData, fr.irisa.cairn.gecos.testframework.model.IVersionOperator...)}.
 * This allows for test-method-specific customizations. 
 * <li> by overriding {@link #beforeBuildTestFlow()}.
 * </ul>
 * 
 * @see S2STestFlow
 * @see TestFlow
 * @see AbstractTestTemplate
 * @see Stages
 * 
 * @author aelmouss
 */
public abstract class TestFlowTemplate<V extends IVersion> extends AbstractTestTemplate<V> {
	
	protected Map<Class<? extends IData>, TestFlow> testFlowMap;
	
	
	@Override
	protected final void beforeTestMethod() {
		super.beforeTestMethod();
		
		this.testFlowMap = new HashMap<>();
		
		configure();
	}
	
	protected void configure() { }
	
	
	@Override
	protected final ITestStage buildTestFlow() {
		ITestStage testFlow = findTestFlow(data.getClass());
		if(testFlow == null)
			throw new RuntimeException("No testflow supporting data of type " + data.getClass() + " was registered!");
		return testFlow;
	}
	
	protected TestFlow findTestFlow(Class<? extends IData> dataCls) {
		return testFlowMap.get(dataCls);
	}
	
	protected void registerTestFlow(Class<? extends IData> dataCls, TestFlow testFlow) {
		Objects.requireNonNull(testFlow);
		testFlowMap.put(dataCls, testFlow);
	}
	
	protected void unregisterTestFlow(Class<? extends IData> dataCls) {
		testFlowMap.remove(dataCls);
	}
	
}
