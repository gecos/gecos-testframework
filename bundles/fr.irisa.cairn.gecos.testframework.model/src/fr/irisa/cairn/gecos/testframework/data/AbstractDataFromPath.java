package fr.irisa.cairn.gecos.testframework.data;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.file.Path;

import fr.irisa.cairn.gecos.testframework.dataprovider.DataFromPathProvider;
import fr.irisa.cairn.gecos.testframework.model.IData;

/**
 * An {@link IData} implementation that can be provided by the {@link DataFromPathProvider}.
 * 
 * <p> Every {@link AbstractDataFromPath} MUST define the following public static methods:
 * <ul>
 * <li> {@code public static boolean} {@value #IS_VALID_PATH_METHOD_NAME} ({@link Path}):
 *   <br> Must return true is the specified path can be used to create a data of this type.
 * <li> {@code public static <? extends AbstractDataFromPath>}  {@value #CREATE_FROM_PATH_METHOD_NAME}  ({@link Path}):
 *   <br>Must create a data instance of this type from the specified path.
 * </ul>
 * 
 * @author aelmouss
 */
public abstract class AbstractDataFromPath implements IData {

	public static final String IS_VALID_PATH_METHOD_NAME = "isValidPath";
	public static final String CREATE_FROM_PATH_METHOD_NAME = "createFromPath";
	

	protected Path path;
	
	public Path getPath() {
		return path;
	}
	
	@Override
	public String getName() {
		return path.getFileName().toString();
	}

	@Override
	public String toString() {
		return path.toString();
	}
	
	
	public static Method getIsValidPathMethod(Class<? extends AbstractDataFromPath> dataClass) {
		try {
			Method m = dataClass.getMethod(AbstractDataFromPath.IS_VALID_PATH_METHOD_NAME, Path.class);
			int modifiers = m.getModifiers();
			if(Modifier.isStatic(modifiers) && Modifier.isPublic(modifiers)) {
				Class<?> retType = m.getReturnType();
				if(boolean.class.equals(retType) || Boolean.class.equals(retType))
					return m;
				else
					throw new NoSuchMethodException("Method return type MUST be 'boolean' or 'Boolean'! " + m);
			} else
				throw new NoSuchMethodException("Method not 'public static'! " + m);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new RuntimeException("Data class does not define a 'public static boolean "+ AbstractDataFromPath.IS_VALID_PATH_METHOD_NAME + " (java.nio.file.Path)' method! " + dataClass, e);
		}
	}
	
	public static Method getCreateFromPathMethod(Class<? extends AbstractDataFromPath> dataClass) {
		try {
			Method m = dataClass.getMethod(AbstractDataFromPath.CREATE_FROM_PATH_METHOD_NAME, Path.class);
			if(Modifier.isStatic(m.getModifiers()) && Modifier.isPublic(m.getModifiers())) {
				Class<?> retType = m.getReturnType();
				if(retType.isAssignableFrom(dataClass))
					return m;
				else
					throw new NoSuchMethodException("Method return type MUST be assignable from " + dataClass + "! " + m);
			} else
				throw new NoSuchMethodException("Method not 'public static'! " + m);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new RuntimeException("Data class does not define a 'public static " + dataClass.getSimpleName() + " " + AbstractDataFromPath.CREATE_FROM_PATH_METHOD_NAME + " (java.nio.file.Path)' method! " + dataClass, e);
		}
	}
	
	public static boolean invokeIsValidPathMethod(Class<? extends AbstractDataFromPath> dataClass, Path path) {
		try {
			Method m = getIsValidPathMethod(dataClass);
			return (boolean) m.invoke(null, path);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	public static <D extends AbstractDataFromPath> D invokeCreateFromPathMethod(Class<D> dataClass, Path path) {
		try {
			Method m = getCreateFromPathMethod(dataClass);
			return (D) m.invoke(null, path);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
