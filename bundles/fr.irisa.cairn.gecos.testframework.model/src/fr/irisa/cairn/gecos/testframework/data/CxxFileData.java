package fr.irisa.cairn.gecos.testframework.data;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

import fr.irisa.cairn.gecos.testframework.exceptions.InvalidDataException;

/**
 * Represent a Data from a single source file.
 * The file name must end with '.c', '.cpp' or '.cc'.
 * 
 * <p> The file is added to the list of sources and
 * the list of include dirs is empty.
 * 
 * @author aelmouss
 */
public class CxxFileData extends AbstractCxxData {
	
	public static boolean isValidPath(Path file) {
		return file != null && Files.isRegularFile(file) 
				&& file.getFileName().toString().matches(FileNamePatterns.CXX_SRC);
	}
	
	public static CxxFileData createFromPath(Path file) throws InvalidDataException {
		if(!isValidPath(file))
			throw new InvalidDataException("Not a valid '" + CxxFileData.class.getSimpleName() + "' data: " + file);
		
		CxxFileData data = new CxxFileData();
		data.path = file;
		data.srcFiles = Arrays.asList(file);
		data.incDirs = Arrays.asList();
		return data;
	}
	
}
