package fr.irisa.cairn.gecos.testframework.s2s;

import java.nio.file.Path;
import java.util.Collection;
import java.util.function.BiFunction;
import java.util.function.Function;

import fr.irisa.cairn.gecos.testframework.exceptions.CompilerFailure;
import fr.irisa.cairn.gecos.testframework.model.IVersion;
import fr.irisa.cairn.gecos.testframework.model.TestFramework;
import fr.irisa.cairn.gecos.testframework.s2s.CompilerOperation.ICompilerFlagBuilder;

/**
 * @author aelmouss
 */
public class Compilers {
	
	private Compilers() {}
	
	public static class GccLikeFlagBuilder implements ICompilerFlagBuilder {
		@Override public String includeDir(String dir) { return "-I" + dir; }
		@Override public String libDir(String dir) { return "-L" + dir; }
		@Override public String lib(String lib) { return "-l" + lib; }
		@Override public String outputFile(String file) { return "-o " + file; }
		@Override public String macro(String name, String value) { return "-D" + name + "='" + value + "'"; }
		@Override public String noLink() { return "-c"; }
		@Override public String noAssemble() { return "-S"; }
	}
	
	public static final ICompilerFlagBuilder GccLikeFlagsBuilder = new GccLikeFlagBuilder();
	
	public static final <V extends IVersion> Path defaultCompilerOutputProvider(V v, Path src) {
		return v.getOutputDir().resolve(src.getFileName().toString()+".o");
	}
	
	public static final <V extends IVersion> Collection<Path> defaultCompilerIncDirsProvider(V v, Path src) {
		return v.getIncludeDirs();
	}
	
	public static final <V extends IVersion> String defaultCompilerFlagsProvider(V v, Path src) {
		return "";
	}
	
	/**
	 * Create a {@link CompilerOperation}.
	 * 
	 * @param compilerProgram the COMPILER shell program command.
	 * @param flagBuilder specify how the compiler flags are constructed.
	 * @param sourceFilesProvider specify which source files of a given version to compile.
	 * @param outputFileProvider specify the output OBJECT file location for a given source file.
	 * @param includeDirsProvider specify the include directories to use for a given source file.
	 * @param additionalFlagsProvider specify any additional flags to use for a given source file.
	 * 
	 * @return a compiler operation.
	 * @throws CompilerFailure if compilation fails when stage is applied.
	 */
	public static <V extends IVersion> CompilerOperation<V> createCompiler(String compilerProgram,
			ICompilerFlagBuilder flagBuilder,
			Function<V, Collection<Path>> sourceFilesProvider,
			BiFunction<V, Path, Path> outputFileProvider,
			BiFunction<V, Path, Collection<Path>> includeDirsProvider,
			BiFunction<V, Path, String> additionalFlagsProvider) {
		return new CompilerOperation<>(compilerProgram, flagBuilder, sourceFilesProvider, 
				outputFileProvider, includeDirsProvider, additionalFlagsProvider);
	}
	
	/**
	 * Create a {@link CompilerOperation} with:
	 * <ul>
	 * <li> {@link #GccLikeFlagsBuilder} as flags builder,
	 * <li> {@code (v, src) ->} {@link IVersion#getOutputDir() v.getCodegenDir()}/src.getFileName().o
	 *  as output file provider,
	 * <li> {@code (v, src) ->} {@link IVersion#getIncludeDirs() v.getIncludeDirs()} as include dirs provider,
	 * <li> no additional flags.
	 * </ul>
	 * <p><b>NOTE:</b> you can use {@link CompilerOperation}.change* methods to create compilers 
	 * with different providers.
	 * 
	 * @param compilerProgram the COMPILER shell program command.
	 * @param sourceFilesProvider specify which source files of a given version to compile.
	 * @return a compiler operation.
	 */
	public static final <V extends IVersion> CompilerOperation<V> createCompiler(String compilerProgram,
			Function<V, Collection<Path>> sourceFilesProvider) {
		return Compilers.<V>createCompiler(compilerProgram, GccLikeFlagsBuilder, sourceFilesProvider, 
				Compilers::defaultCompilerOutputProvider,
				Compilers::defaultCompilerIncDirsProvider, 
				Compilers::defaultCompilerFlagsProvider);
	}
	
	/**
	 * Create a {@link CompilerOperation} with:
	 * <ul>
	 * <li> all generated source files ({@link IVersion#getGeneratedSourceFiles()}) as source files provider.
	 * <li> {@link #GccLikeFlagsBuilder} as flags builder,
	 * <li> {@code (v, src) ->} {@link IVersion#getOutputDir() v.getCodegenDir()}/src.getFileName().o
	 *  as output file provider,
	 * <li> {@code (v, src) ->} {@link IVersion#getIncludeDirs() v.getIncludeDirs()} as include dirs provider,
	 * <li> no additional flags.
	 * </ul>
	 * <p><b>NOTE:</b> you can use {@link CompilerOperation}.change* methods to create a new compilers 
	 * with different providers.
	 * 
	 * @param compilerProgram the COMPILER shell program command.
	 * @return a compiler operation.
	 */
	public static final <V extends IVersion> CompilerOperation<V> createCompiler(String compilerProgram) {
		return Compilers.<V>createCompiler(compilerProgram, GccLikeFlagsBuilder, 
				v -> v.getGeneratedSourceFiles(), 
				Compilers::defaultCompilerOutputProvider,
				Compilers::defaultCompilerIncDirsProvider, 
				Compilers::defaultCompilerFlagsProvider);
	}
	
	/**
	 * Shorthand for {@link #createCompiler(String) createCompiler()}
	 * with "cc" as compiler program.
	 * 
	 * @return a compiler operation.
	 */
	public static final <V extends IVersion> CompilerOperation<V> cc() {
		return createCompiler(TestFramework.findProgram("cc"));
	}
	
	/**
	 * Shorthand for {@link #createCompiler(String) createCompiler()}
	 * with "gcc" as compiler program.
	 * 
	 * @return a compiler operation.
	 */
	public static final <V extends IVersion> CompilerOperation<V> gcc() {
		return createCompiler(TestFramework.findProgram("gcc"));
	}
	
	/**
	 * Shorthand for {@link #createCompiler(String) createCompiler()}
	 * with "gcc" as compiler program and "-std=c99 -pedantic" as 
	 * additional flags.
	 * 
	 * @return a compiler operation.
	 */
	public static final <V extends IVersion> CompilerOperation<V> gccC99() {
		return Compilers.<V>createCompiler(TestFramework.findProgram("gcc"))
				.changeFlagsProvider((v,s) -> "-std=c99 -pedantic");
	}
	
	/**
	 * Shorthand for {@link #createCompiler(String) createCompiler()}
	 * with "clang" as compiler program.
	 * 
	 * @return a compiler operation.
	 */
	public static final <V extends IVersion> CompilerOperation<V> clang() {
		return createCompiler(TestFramework.findProgram("clang"));
	}
	
	/**
	 * Shorthand for {@link #createCompiler(String) createCompiler()}
	 * with "mpicc" as compiler program.
	 * 
	 * @return a compiler operation.
	 */
	public static final <V extends IVersion> CompilerOperation<V> mpicc() {
		return createCompiler(TestFramework.findProgram("mpicc"));
	}
}
