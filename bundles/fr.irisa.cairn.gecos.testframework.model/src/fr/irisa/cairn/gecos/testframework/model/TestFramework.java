package fr.irisa.cairn.gecos.testframework.model;

import java.io.File;
import java.nio.file.Path;

import fr.irisa.r2d2.gecos.framework.utils.AbstractShell;
import fr.irisa.r2d2.gecos.framework.utils.SystemExec;
import fr.irisa.r2d2.gecos.framework.utils.TimeoutException;

public class TestFramework {

	private static final boolean verbose = false;
	
	public static AbstractShell shell = SystemExec.createShell().setVerbose(verbose);
	
		
	/**
	 * Invoke.
	 *
	 * @param cmd the cmd
	 * @return the int
	 */
	public static int invoke(String cmd) {
		return invoke(cmd, null, null, null);
	}
	
	/**
	 * Invoke.
	 *
	 * @param cmd the cmd
	 * @param timeout the timeout
	 * @return the int
	 * @throws TimeoutException the timeout exception
	 */
	public static int invoke(String cmd, Long timeout) throws TimeoutException {
		return invoke(cmd, timeout, null, null);
	}
	
	/**
	 * Invoke.
	 *
	 * @param cmd shell command to be executed.
	 * @param timeout time in seconds after which the {@code cmd} is killed,
	 * 	or {@code null} to disable timeout. If timeout occurs this returns with
	 *  timeout exit code of the shell {@link #shell}.
	 * @param stdoutFile if not {@code null}, stdout is redirected to this file.
	 * @param stderrFile if not {@code null}, stderr is redirected to this file.
	 * @return exit code
	 * @throws TimeoutException if timeout
	 * @see SystemExec#shell(String, Long, boolean, boolean, File, File)
	 */
	public static int invoke(String cmd, Long timeout, Path stdoutFile, Path stderrFile) throws TimeoutException  {
		return shell.shell(cmd, timeout, AbstractShell.outToFile(stdoutFile), AbstractShell.outToFile(stderrFile));
	}

	/**
	 * Try to locate the full path of the specified command.
	 *
	 * @param cmd the name of command to locate
	 * @return the command full path if found, otherwise return the input {@code cmd}.
	 */
	public static String findProgram(String cmd) {
		String path = shell.findProgram(cmd);
		return path == null? cmd : path;
	}
	
}
