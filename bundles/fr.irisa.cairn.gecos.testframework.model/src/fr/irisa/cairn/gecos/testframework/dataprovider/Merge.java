package fr.irisa.cairn.gecos.testframework.dataprovider;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import fr.irisa.cairn.gecos.testframework.dataprovider.DataProviderFilter.NO_FILTERING;
import fr.irisa.cairn.gecos.testframework.model.IData;
import fr.irisa.cairn.gecos.testframework.model.IDataFilter;

/**
 * Used by {@link AbstractDataProvider} to merge in data from another
 * {@link AbstractDataProvider} specified by {@link #location()}.
 * 
 * <p> Data from the latter can be optionally filtered and/or limited.
 * 
 * <p> Defaults to no filtering and no limiting.
 * 
 * @author aelmouss
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Repeatable(MergeList.class)
public @interface Merge {

	/**
	 * DataProvider class.
	 *
	 * @return the DataProvider class
	 */
	Class<? extends AbstractDataProvider<? extends IData>> location();

	/**
	 * Optional Filter class to filter the data provided by the specified provider.
	 * Only works with {@link AbstractDataProvider} providers.
	 * 
	 * <p> Default to {@link NO_FILTERING}.
	 *
	 * @return the filter class
	 */
	Class<? extends IDataFilter<? extends IData>> filter() default DataProviderFilter.NO_FILTERING.class ;
	
	/**
	 * Optional maximum number of data to be provided by the specified provider.
	 * Only works with {@link AbstractDataProvider} providers.
	 * 
	 * <p> No limit is applied if negative.
	 * 
	 * <p> Defaults to -1 (i.e. no limit).
	 * 
	 * @return the maximum number of data to provide.
	 */
	int limit() default -1;

}

