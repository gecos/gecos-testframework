package fr.irisa.cairn.gecos.testframework.stages;

import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import fr.irisa.cairn.gecos.testframework.exceptions.StopException;
import fr.irisa.cairn.gecos.testframework.exceptions.CreationFailure;
import fr.irisa.cairn.gecos.testframework.model.AbstractTestTemplate;
import fr.irisa.cairn.gecos.testframework.model.IData;
import fr.irisa.cairn.gecos.testframework.model.ITestStage;
import fr.irisa.cairn.gecos.testframework.model.IVersion;
import fr.irisa.cairn.gecos.testframework.model.IVersionBiOperator;
import fr.irisa.cairn.gecos.testframework.model.IVersionOperator;
import fr.irisa.cairn.gecos.testframework.s2s.Comparators;
import fr.irisa.cairn.gecos.testframework.s2s.Compilers;
import fr.irisa.cairn.gecos.testframework.s2s.Linkers;
import fr.irisa.cairn.gecos.testframework.s2s.Operators;

/**
 * A utility class to help create test {@link ITestStage stages}.
 * 
 * @see Compilers
 * @see Linkers
 * @see Operators
 * @see Comparators
 * 
 * @author aelmouss
 */
public class Stages {

	private Stages() {}
	
	
	/**
	 * Create a stage that when applied, it sets the list of version of the
	 * specified test template (target) using {@code versionsSupplier}.
	 * 
	 * @param versionsSupplier create versions
	 * @return new test stage.
	 */
	public static <V extends IVersion> ITestStage supplier(Supplier<List<V>> versionsSupplier) {
		return target -> {
			@SuppressWarnings("unchecked")
			AbstractTestTemplate<V> testTemplate = (AbstractTestTemplate<V>)target;
			testTemplate.setVersions(versionsSupplier.get());
		};
	}
	
	/**
	 * Create the original version from the test template's data.
	 * The created version (v0) is added to the template's list of versions.
	 * 
	 * @param dataConvertor a function to apply on data to create v0.
	 * @return a new stage.
	 */
	@SuppressWarnings("unchecked")
	public static <D extends IData, V extends IVersion> ITestStage convert(
			Function<D, V> dataConvertor) {
		Objects.requireNonNull(dataConvertor);
		return target -> {
			AbstractTestTemplate<V> testTemplate = (AbstractTestTemplate<V>)target;
			D data = (D) testTemplate.getData();
			List<V> versions = testTemplate.getVersions();
			try {
				V original = dataConvertor.apply(data);
				versions.add(original);
			} catch (Exception e) {
				throw new CreationFailure("Failed creating original version.", e);
			}
		};
	}
	
	/**
	 * Create a stage that when applied,
	 * create the transformed versions by applying the test template's 
	 * transformations on its original version (v0).
	 * 
	 * <p> Depending of the value of {@code sequential}, the transformations
	 * are applied either:
	 * <ul>
	 * <li> Sequentially; each transformation (Ti) is applied on a copy of the 
	 * result of the previous one, starting from v0: <i> vi+1 = Ti(copy(vi)) </i>
	 * <li> Or, on a copy of v0: <i> vi+1 = Ti(copy(<b>v0</b>)) </i>
	 * </ul>
	 * 
	 * <p> All created versions are added, in original-first order, to the
	 * versions list of the test template. 
	 * 
	 * @param versionCopier create a copy of the provided version.
	 * @param sequential if true, transformations are applied sequentially.
	 * Otherwise, each is applied on a copy of the original version.
	 * 
	 * @return new {@link TransformationStage}.
	 */
	public static <V extends IVersion> TransformationStage<V> transform(
			Function<V, V> versionCopier, boolean sequential) {
		return new TransformationStage<>(versionCopier, sequential);
	}
	
	/**
	 * Shorthand for {@link #transform(Function, boolean)} with sequential mode.
	 * 
	 * @return new {@link TransformationStage}.
	 */
	public static <V extends IVersion> TransformationStage<V> transform(
			Function<V, V> versionCopier) {
		return transform(versionCopier, true);
	}
	
	/**
	 * When this stage is run, apply {@code ifTrueOperator} on each version for which
	 * {@code condition} evaluates to {@code true}.
	 * 
	 * <p> Otherwise, if {@code ifFalseOperator} is not {@code null}, apply it on the 
	 * remaining versions (i.e. for which {@code condition} evaluates to {@code false}.
	 * 
	 * <p> A {@code null} {@code condition} is equivalent to {@code v -> true} i.e.
	 * {@code ifTrueOperator} is applied on all versions.
	 * 
	 * @param parallel if true the operation is applied on version in parallel.
	 * @param condition if {@code null}, @code ifTrueOperator} is applied on all versions.
	 * @param ifTrueOperator to apply of versions passing condition. Cannot be {@code null}.
	 * @param ifFalseOperator to apply of versions not passing condition. If {@code null}
	 * no operation is applied on such versions.
	 * 
	 * @return new test stage.
	 */
	public static <V extends IVersion> ITestStage forEach(
			boolean parallel, Predicate<V> condition, 
			IVersionOperator<V> ifTrueOperator, IVersionOperator<V> ifFalseOperator) {
		return target -> {
			@SuppressWarnings("unchecked")
			AbstractTestTemplate<V> testTemplate = (AbstractTestTemplate<V>)target;
			Stream<V> versions = testTemplate.getVersions().stream();
			if(parallel)
				versions = versions.parallel();
			versions.forEach(v -> {
				IVersionOperator<V> operator = condition == null ? ifTrueOperator :
						condition.test(v) ? ifTrueOperator : ifFalseOperator;
				operator.apply(v);
			});
		};
	}
	
	/**
	 * Shorthand to {@link #forEach(boolean, Predicate, IVersionOperator, IVersionOperator)}
	 * with no parallel.
	 */
	public static <V extends IVersion> ITestStage forEach(Predicate<V> condition, 
			IVersionOperator<V> ifTrueOperator, IVersionOperator<V> ifFalseOperator) {
		return forEach(false, condition, ifTrueOperator, ifFalseOperator);
	}
	
	/**
	 * When this stage is run, apply {@code ifTrueOperator} only on versions for which
	 * {@code condition} evaluates to {@code true}.
	 * 
	 * <p> A {@code null} {@code condition} is equivalent to {@code v -> true} i.e.
	 * {@code ifTrueOperator} is applied on all versions.
	 * 
	 * @param condition if {@code null}, @code ifTrueOperator} is applied on all versions.
	 * @param ifTrueOperator to apply of versions passing condition. Cannot be {@code null}.
	 * 
	 * @return new test stage.
	 */
	public static <V extends IVersion> ITestStage forEach(Predicate<V> condition, 
			IVersionOperator<V> ifTrueOperator) {
		return forEach(false, condition, ifTrueOperator, null);
	}

	/**
	 * When this stage is run, apply the specified {@code operator} on all versions.
	 * 
	 * @return new test stage.
	 */
	public static <V extends IVersion> ITestStage forEach(
			IVersionOperator<V> operator) {
		return forEach(false, null, operator, null);
	}
	
	/**
	 * Similar to {@link #forEach(boolean, Predicate, IVersionOperator, IVersionOperator)}
	 * but the test template (i.e. target) is passed to the operator in addition to the version.
	 */
	public static <V extends IVersion, T extends AbstractTestTemplate<V>> ITestStage forEach2(
			boolean parallel, BiPredicate<T,V> condition,
			BiConsumer<T,V> ifTrueOperator, BiConsumer<T,V> ifFalseOperator) {
		return target -> {
			@SuppressWarnings("unchecked")
			T testTemplate = (T)target;
			Stream<V> versions = testTemplate.getVersions().stream();
			if(parallel)
				versions = versions.parallel();
			versions.forEach(v -> {
				BiConsumer<T,V> operator = condition == null ? ifTrueOperator :
						condition.test(testTemplate,v) ? ifTrueOperator : ifFalseOperator;
				operator.accept(testTemplate,v);
			});
		};
	}
	
	/**
	 * Shorthand to {@link #forEach2(boolean, Predicate, IVersionOperator, IVersionOperator)}
	 * with no parallel.
	 */
	public static <V extends IVersion, T extends AbstractTestTemplate<V>> ITestStage forEach2(
			BiPredicate<T,V> condition, BiConsumer<T,V> ifTrueOperator, BiConsumer<T,V> ifFalseOperator) {
		return forEach2(false, condition, ifTrueOperator, ifFalseOperator);
	}
	
	/**
	 * When this stage is run, apply {@code ifTrueOperator} only on versions for which
	 * {@code condition} evaluates to {@code true}.
	 * 
	 * <p> A {@code null} {@code condition} is equivalent to {@code v -> true} i.e.
	 * {@code ifTrueOperator} is applied on all versions.
	 * 
	 * @param condition if {@code null}, @code ifTrueOperator} is applied on all versions.
	 * @param ifTrueOperator to apply of versions passing condition. Cannot be {@code null}.
	 * 
	 * @return new test stage.
	 */
	public static <V extends IVersion, T extends AbstractTestTemplate<V>> ITestStage forEach2(
			BiPredicate<T,V> condition,	BiConsumer<T,V> ifTrueOperator) {
		return forEach2(false, condition, ifTrueOperator, null);
	}

	/**
	 * When this stage is run, apply the specified {@code operator} on all versions.
	 * 
	 * @return new test stage.
	 */
	public static <V extends IVersion, T extends AbstractTestTemplate<V>> ITestStage forEach2(
			BiConsumer<T,V> operator) {
		return forEach2(false, null, operator, null);
	}
	
	//TODO add forEachWithIdx() methods ...
	
	/**
	 * When this stage is run, apply {@code biOperation} on each version pair.
	 * <br> Pairs (v1, v2) with indexes i1, i2 respectively are obtained as following:
	 * <ul>
	 * <li> i1 takes integer values in [0, size-1], with increments of +1 (i1++)
	 * and size being the total number of versions.
	 * <li> i2 = v2IndexFunction(i1)
	 * <li> a pair is skipped if i2 >= size or i2 < 0 or i2 == i1
	 * </ul>
	 * 
	 * @param biOperation operation to apply on each version pair. Cannot be {@code null}.
	 * @param v2IndexFunction index function, applied on the index of v1 to obtain index of v2.
	 * i.e. for index i, the pair (versions[i], versions[ v2IndexFunction(i) ]) is passed
	 * to {@code biOperator}.
	 * 
	 * @return new {@link BiOperationStage}.
	 */
	public static <V extends IVersion> BiOperationStage<V> forEachPair(
			IVersionBiOperator<V> biOperation,
			UnaryOperator<Integer> v2IndexFunction) {
		return new BiOperationStage<>(biOperation, v2IndexFunction);
	}
	
	/**
	 * Shorthand to {@link #forEachPair(IVersionBiOperator, UnaryOperator)}, with
	 * i -> i+1 as v2IndexFunction.
	 * <br> That is, the provided pairs are: (v0,v1), (v1,v2) .. (vi, vi+1) ..
	 * 
	 * @param biOperation operation to apply on each version pair. Cannot be {@code null}.
	 * 
	 * @return new {@link BiOperationStage}.
	 */
	public static <V extends IVersion> BiOperationStage<V> forEachPairSeq(
			IVersionBiOperator<V> biOperation) {
		return new BiOperationStage<>(biOperation, i -> i+1);
	}
	
	/**
	 * Shorthand to {@link #forEachPair(IVersionBiOperator, UnaryOperator)}, with
	 * i -> 0 as v2IndexFunction.
	 * <br> That is, the provided pairs are: (v1,v0), (v2,v0) .. (vi, v0) ..
	 * 
	 * @param biOperation operation to apply on each version pair. Cannot be {@code null}.
	 * 
	 * @return new {@link BiOperationStage}.
	 */
	public static <V extends IVersion> BiOperationStage<V> forEachPairWithFirst(
			IVersionBiOperator<V> biOperation) {
		return new BiOperationStage<>(biOperation, i -> 0);
	}
	
	/**
	 * Create a stage that when applied, the specified {@code versionsOperation}
	 * is applied on the test template's (target) list of versions.
	 * 
	 * @param versionsOperation operation to apply.
	 * @return new test stage.
	 */
	public static <V extends IVersion> ITestStage forAll(Consumer<List<V>> versionsOperation) {
		return target -> {
				@SuppressWarnings("unchecked")
				AbstractTestTemplate<V> testTemplate = (AbstractTestTemplate<V>)target;
				List<V> versions = testTemplate.getVersions();
				versionsOperation.accept(versions);
		};
	}
	
	/**
	 * Create a stage chain which will apply the specified stages
	 * in the same order (first stage will be applied first).
	 * 
	 * @param stages to be chained
	 * @return a new {@link SequenceStage} with the specified stages.
	 */
	public static final SequenceStage chain(ITestStage... stages) {
		return new SequenceStage(stages);
	}
		
	public static final SequenceStage chain(List<ITestStage> stages) {
		return chain(stages.toArray(new ITestStage[stages.size()]));
	}
	
	/**
	 * Create a stage that when applied, it evaluate the specified {@code condition}
	 * and if true, the {@code ifTrueStage} is applied, otherwise {@code ifFalseStage}.
	 * 
	 * @param condition given the test template as input, decides whether to apply
	 *  {@code ifTrueStage} or {@code ifFalseStage}.
	 * @param ifTrueStage applied if condition evaluates to {@code true}.
	 * @param ifFalseStage applied if condition evaluates to {@code false}.
	 * @return a new stage.
	 */
	public static ITestStage branch(Predicate<Object> condition, ITestStage ifTrueStage, 
			ITestStage ifFalseStage) {
		Objects.requireNonNull(condition);
		Objects.requireNonNull(ifTrueStage);
		Objects.requireNonNull(ifFalseStage);
		return target -> {
			if(condition.test(target))
				ifTrueStage.apply(target);
			else
				ifFalseStage.apply(target);
		};
	}
	
	/**
	 * Create a stage that when applied, it evaluate the specified {@code condition}
	 * and only if true, the {@code ifTrueStage} is applied.
	 * 
	 * @param condition given the test template as input, decides whether to apply
	 *  {@code ifTrueStage} or do nothing.
	 * @param ifTrueStage applied if condition evaluates to {@code true}.
	 * @return a new stage.
	 */
	public static ITestStage onlyIf(Predicate<Object> condition, ITestStage ifTrueStage) {
		Objects.requireNonNull(condition);
		Objects.requireNonNull(ifTrueStage);
		return target -> {
			if(condition.test(target))
				ifTrueStage.apply(target);
		};
	}
	
//    public static <V extends IVersion> List<ITestStage> findStagesByTag(
//    		ITestStage inStage, String tag) {
//		//TODO
//	}
	
	/**
	 * A stage that does nothing.
	 * Used to disable a stage for instance.
	 */
	public static final ITestStage NOP = t -> {};
	
	/**
	 * When this stage is applied the test flow is stopped and marked as successful 
	 * (i.e. not skip nor failure) unless later catchers decide otherwise.
	 */
	public static final ITestStage STOP = t -> {throw new StopException("STOP satge encountered");};
	
	
	
}
