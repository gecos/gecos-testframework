package fr.irisa.cairn.gecos.testframework.s2s;

import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.CHECK;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.CODEGEN;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.COMPILE;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.CONVERT;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.LINK;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.RUN;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.TRANSFORM;
import static fr.irisa.cairn.gecos.testframework.s2s.S2STestFlow.S2SStageName.VERIFY;

import fr.irisa.cairn.gecos.testframework.model.ITestStage;
import fr.irisa.cairn.gecos.testframework.s2s.TestFlow.IStageName;

public class S2STestFlow {

	public enum S2SStageName implements IStageName {
		START    ,
		CONVERT  ,
		TRANSFORM,
		CHECK    ,
		CODEGEN  ,
		COMPILE  ,
		LINK     ,
		RUN      ,
		VERIFY;
	}
	
	/**
	 * Create a {@link TestFlow} builder using names specified by
	 * {@link S2SStageName}.
	 * 
	 * @return a new builder
	 */
	public static S2SFlowBuilder builder() {
		return new S2SFlowBuilder(S2SStageName.values());
	}
	
	public static class S2SFlowBuilder extends TestFlowBuilder {
		
		protected S2SFlowBuilder(IStageName[] names) {
			super(names);
		}
		
		@Override
		public S2SFlowBuilder named(IStageName name, ITestStage stage) {
			return (S2SFlowBuilder) super.named(name, stage);
		}
		
		@Override
		public S2SFlowBuilder then(ITestStage stage) {
			return (S2SFlowBuilder) super.then(stage);
		}
		
		public S2SFlowBuilder convert(ITestStage stage) {
			return named(CONVERT, stage);
		}
		public S2SFlowBuilder transform(ITestStage stage) {
			return named(TRANSFORM, stage);
		}
		public S2SFlowBuilder check(ITestStage stage) {
			return named(CHECK, stage);
		}
		public S2SFlowBuilder codegen(ITestStage stage) {
			return named(CODEGEN, stage);
		}
		public S2SFlowBuilder compile(ITestStage stage) {
			return named(COMPILE, stage);
		}
		public S2SFlowBuilder link(ITestStage stage) {
			return named(LINK, stage);
		}
		public S2SFlowBuilder run(ITestStage stage) {
			return named(RUN, stage);
		}
		public S2SFlowBuilder verify(ITestStage stage) {
			return named(VERIFY, stage);
		}
	}
}
