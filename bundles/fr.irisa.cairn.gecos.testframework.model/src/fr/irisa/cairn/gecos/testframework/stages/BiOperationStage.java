package fr.irisa.cairn.gecos.testframework.stages;

import java.util.List;
import java.util.Objects;
import java.util.function.UnaryOperator;

import fr.irisa.cairn.gecos.testframework.model.AbstractTestTemplate;
import fr.irisa.cairn.gecos.testframework.model.ITestStage;
import fr.irisa.cairn.gecos.testframework.model.IVersion;
import fr.irisa.cairn.gecos.testframework.model.IVersionBiOperator;

/**
 * Apply a binary operation on version pairs from the 
 * versions list in the test template.
 * 
 * @author aelmouss
 */
public class BiOperationStage<V extends IVersion> implements ITestStage {
	
	protected IVersionBiOperator<V> biOperator;
	protected UnaryOperator<Integer> idxFun;
	
	public BiOperationStage(IVersionBiOperator<V> biOperator, UnaryOperator<Integer> v2Index) {
		Objects.requireNonNull(biOperator);
		Objects.requireNonNull(v2Index);
		this.biOperator = biOperator;
		this.idxFun = v2Index;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public void apply(Object target) {
		AbstractTestTemplate<V> testTemplate = (AbstractTestTemplate<V>)target;
		List<V> versions = testTemplate.getVersions();
		int size = versions.size();
		
		for(int i = 0; i < size; i++) {
			int i2 = idxFun.apply(i);
			if(i2 >= size || i2 < 0 || i2 == i)
				continue;
			
			biOperator.apply(versions.get(i), versions.get(i2));
		}
	}
	
 }