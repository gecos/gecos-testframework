package fr.irisa.cairn.gecos.testframework.model;

import fr.irisa.cairn.gecos.testframework.exceptions.OperationFailureException;

/**
 * @author aelmouss
 */
public interface IVersionOperator<V extends IVersion> {

	/**
	 * @param version to be checked.
	 * @throws OperationFailureException in case the check failed
	 */
	public void apply(V version) throws OperationFailureException;

}
