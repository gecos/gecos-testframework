package fr.irisa.cairn.gecos.testframework.s2s;

import static fr.irisa.cairn.gecos.testframework.utils.OperationUtils.executeCmd;
import static fr.irisa.cairn.gecos.testframework.utils.OperationUtils.join;
import static java.util.Collections.emptyList;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.function.Function;

import fr.irisa.cairn.gecos.testframework.exceptions.RunnerFailure;
import fr.irisa.cairn.gecos.testframework.exceptions.OperationFailureException;
import fr.irisa.cairn.gecos.testframework.model.IVersion;
import fr.irisa.cairn.gecos.testframework.model.IVersionOperator;
import fr.irisa.cairn.gecos.testframework.model.TestFramework;

public class Operators {

	private Operators() { }
	
	/**
	 * Create an operation that when applied it executes the following shell command,
	 * for a given version v:
	 * <br><i><code> 
	 * programProvider(v) argumentsProvider(v)
	 * </code></i>
	 * Then store its exit code in {@link IVersion#setExitCode(int) version}.
	 * 
	 * @param programProvider
	 * @param argumentsProvider
	 * @throws RunnerFailure if execution fails.
	 * @return a new executor operation.
	 */
	public static final <V extends IVersion> IVersionOperator<V> createExecutor(
			Function<V, String> programProvider, 
			Function<V, Collection<String>> argumentsProvider) {
		return new IVersionOperator<V>() {
			@Override
			public void apply(V v) {
				String cmd = programProvider.apply(v) + " " + join(argumentsProvider.apply(v), s->s);
				try {
					int res = executeCmd(cmd, v.getTimeout(), v.getStdoutFile(), v.getStderrFile());
					v.setExitCode(res);
				} catch (Exception e) {
					throw new RunnerFailure("Failed running version '" + v + "'", e);
				}
			}
		};
	}
	
	/**
	 * Shorthand to {@link #createExecutor(Function, Function)} with :
	 * <ul>
	 * <li> version {@link IVersion#getExecutable() executable} absolute path as
	 * Program provider.
	 * <li> argumentsProvider as arguments provider
	 * </ul>
	 * 
	 * @param argumentsProvider
	 * @return a new executor.
	 */
	public static final <V extends IVersion> IVersionOperator<V> defaultExecutor(
			Function<V, Collection<String>> argumentsProvider) {
		return createExecutor(v -> v.getExecutable().toAbsolutePath().toString(), argumentsProvider);
	}
	
	/**
	 * Shorthand to {@link #createExecutor(Function, Function)} with :
	 * <ul>
	 * <li> version {@link IVersion#getExecutable() executable} absolute path as
	 * Program command.
	 * <li> no arguments.
	 * </ul>
	 * 
	 * @param argumentsProvider
	 * @return a new executor.
	 */
	public static final <V extends IVersion> IVersionOperator<V> defaultExecutorNoArgs() {
		return createExecutor(v -> v.getExecutable().toAbsolutePath().toString(), 
				v -> emptyList());
	}
	
	/**
	 * Create an executor that runs the following command when applied,
	 * for a given version v:
	 * <br><i><code> 
	 * makeProgram -f makeFileProvider(v) makeTargetProvider(v) otherArgumentsProvider(v)
	 * </code></i>
	 * 
	 * @param makeProgram
	 * @param makeFileProvider
	 * @param makeTargetProvider
	 * @param otherArgumentsProvider
	 * @return a new executor.
	 */
	public static final <V extends IVersion> IVersionOperator<V> makefileExecutor(String makeProgram,
			Function<V, Path> makeFileProvider,
			Function<V, String> makeTargetProvider,
			Function<V, Collection<String>> otherArgumentsProvider) {
		return createExecutor(
				v -> makeProgram + " -f " + makeFileProvider.apply(v) + " " + makeTargetProvider.apply(v),
				otherArgumentsProvider); 
	}
	
	/**
	 * Shorthand to {@link #makefileExecutor(Function, Function, Function)}
	 * with the shell defined "make" as  makeProgram.
	 * 
	 * @param makeFileProvider
	 * @param makeTargetProvider
	 * @param otherArgumentsProvider
	 * @return a new executor.
	 */
	public static final <V extends IVersion> IVersionOperator<V> makefileExecutor(
			Function<V, Path> makeFileProvider,
			Function<V, String> makeTargetProvider,
			Function<V, Collection<String>> otherArgumentsProvider) {
		return createExecutor(
				v -> TestFramework.findProgram("make") + " -f " + makeFileProvider.apply(v) 
					+ " " + makeTargetProvider.apply(v),
				otherArgumentsProvider); 
	}
	
	/**
	 * Shorthand to {@link #makefileExecutor(Function, Function, Function)}
	 * with the shell defined "make" as  makeProgram and empty otherArgumentsProvider. 
	 * 
	 * @param makeFileProvider
	 * @param makeTargetProvider
	 * @return a new executor.
	 */
	public static final <V extends IVersion> IVersionOperator<V> makefileExecutor(
			Function<V, Path> makeFileProvider,
			Function<V, String> makeTargetProvider) {
		return createExecutor(
				v -> TestFramework.findProgram("make") + " -f " + makeFileProvider.apply(v) 
					+ " " + makeTargetProvider.apply(v),
				v -> emptyList()); 
	}
	
	/**
	 * Create a operation that when applied it execute the following shell command,
	 * for a given version v:
	 * <br><i><code> 
	 *  mpirun -np nbProcesses(v) otherOptionsAndExecutable(V)
	 * </code></i>
	 * 
	 * @param nbProcesses
	 * @param otherOptionsAndExecutable
	 * @return a new operator.
	 */
	public static final <V extends IVersion> IVersionOperator<V> mpiExecutor(
			Function<V, Integer> nbProcesses,
			Function<V, Collection<String>> otherOptionsAndExecutable) {
		return createExecutor(v -> TestFramework.findProgram("mpirun -np " + nbProcesses.apply(v).toString()),
				otherOptionsAndExecutable); 
	}
	
	/**
	 * Create an operation that when applied, generates a file at the location
	 * specified by {@code filePathProvider} and containing the content provided
	 * by {@code contentProvider}.
	 * 
	 * @param filePathProvider specify where the file will be created.
	 * @param contentProvider specify the content to be written. 
	 * @return a new operation.
	 */
	public static final <V extends IVersion> IVersionOperator<V> fileGenerator(
			Function<V, Path> filePathProvider,
			Function<V, String> contentProvider) {
		return v -> {
			Path path = filePathProvider.apply(v);
//			if(Files.exists(path)) {
//				//TODO error or append ??
//			}
			String content = contentProvider.apply(v);
			try {
				Files.write(path, content.getBytes());
			} catch(IOException e) {
				throw new OperationFailureException("Failed to generate file '" + path + "'", e);
			}
		};
	}
}
