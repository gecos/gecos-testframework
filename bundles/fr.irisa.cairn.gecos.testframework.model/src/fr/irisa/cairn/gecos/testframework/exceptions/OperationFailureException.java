package fr.irisa.cairn.gecos.testframework.exceptions;

public class OperationFailureException extends RuntimeException {

	private static final long serialVersionUID = -3331813459884797796L;

	public OperationFailureException(String msg) {
		super(msg);
	}

	public OperationFailureException(String string, Exception e) {
		super(string, e);
	}

}
