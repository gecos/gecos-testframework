package fr.irisa.cairn.gecos.testframework.exceptions;

public class CreationFailure extends OperationFailureException {

	private static final long serialVersionUID = -7198418202419297040L;

	public CreationFailure(String msg) {
		super(msg);
	}

	public CreationFailure(String string, Exception e) {
		super(string, e);
	}

}
