package fr.irisa.cairn.gecos.testframework.s2s;

import static fr.irisa.cairn.gecos.testframework.utils.OperationUtils.executeCmd;
import static fr.irisa.cairn.gecos.testframework.utils.OperationUtils.join;
import static java.util.stream.Collectors.joining;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Stream;

import fr.irisa.cairn.gecos.testframework.exceptions.CompilerFailure;
import fr.irisa.cairn.gecos.testframework.model.IVersion;
import fr.irisa.cairn.gecos.testframework.model.IVersionOperator;
import fr.irisa.cairn.gecos.testframework.utils.OperationUtils;

/**
 * This {@link IVersionOperator} implementation compile code of a {@link IVersion}
 * by executing shell commands invoking a system compiler (e.g. gcc, g++).
 * 
 * @author aelmouss
 */
public class CompilerOperation<V extends IVersion> implements IVersionOperator<V> {
	
	public interface ICompilerFlagBuilder {
		public String includeDir(String dir);
		public String libDir(String dir);
		public String lib(String lib);
		public String outputFile(String file);
		public String macro(String name, String value);
		public String noLink();
		public String noAssemble();
	}

	protected String compilerProgram;
	protected ICompilerFlagBuilder flagBuilder;
	protected Function<V, Collection<Path>> sourcesProvider;
	protected BiFunction<V, Path, Path> outputFileProvider;
	protected BiFunction<V, Path, Collection<Path>> incDirsProvider;
	protected BiFunction<V, Path, String> additionalFlagsProvider;

	
	public CompilerOperation(String compilerProgram, ICompilerFlagBuilder flagBuilder,
			Function<V, Collection<Path>> sourcesProvider,
			BiFunction<V, Path, Path> outputFileProvider,
			BiFunction<V, Path, Collection<Path>> includeDirsProvider,
			BiFunction<V, Path, String> additionalFlagsProvider) {
		this.compilerProgram = compilerProgram;
		this.flagBuilder = flagBuilder;
		this.sourcesProvider = sourcesProvider;
		this.outputFileProvider = outputFileProvider;
		this.incDirsProvider = includeDirsProvider;
		this.additionalFlagsProvider = additionalFlagsProvider;
	}
	
	/**
	 * Compile, separately, each of the {@code version} generated source files as specified by 
	 * {@link #sourcesProvider}.
	 * 
	 * <p> For each source file, the compilation command is constructed by 
	 * {@link #getCompileCmd(IVersion, Path)}, then executed by 
	 * {@link OperationUtils#executeCmd(String, Long)} with the timeout specified 
	 * by {@link IVersion#getTimeout()}.
	 * 
	 * <p> The generated Object files corresponding to the sources are then set in version
	 * using {@link IVersion#setGeneratedObjects(List)}.
	 * 
	 * @throws CompilerFailure if compilation fails when stage is applied.
	 */
	@Override
	public void apply(V v) {
		try {
			final Long timeout = v.getTimeout();
			List<Path> generatedObjects = new ArrayList<>();
			sourcesProvider.apply(v).stream()
				.peek(src -> generatedObjects .add(outputFileProvider.apply(v, src)))
				.map(src -> getCompileCmd(v, src))
				.forEach(cmd -> executeCmd(cmd, timeout));
			v.setGeneratedObjects(generatedObjects);
		} catch(Exception e) {
			throw new CompilerFailure("Failed compiling version '" + v + "'", e);
		}
	}

	/**
	 * Construct the command to be used to compile the source file {@code src}
	 * from {@code version}.
	 * It uses:
	 * <li> {@link #compilerProgram} as the COMPILER shell program.
	 * <li> {@link #outputFileProvider} to provide to output OBJECT file location.
	 * <li> {@link #incDirsProvider} to provide header files locations 'INC_DIRS'.
	 * <li> {@link #additionalFlagsProvider} to provide any additional FLAGS.
	 * 
	 * @param v to which the source belong
	 * @param src source file to compile
	 * @return "COMPILER FLAGS INC_DIRS -o OBJECT -c src"
	 */
	protected String getCompileCmd(V v, Path src) {
		return Stream.of(
			compilerProgram,
			additionalFlagsProvider.apply(v, src),
			join(incDirsProvider.apply(v, src), d -> flagBuilder.includeDir(d.toString())),
			flagBuilder.outputFile(outputFileProvider.apply(v, src).toString()),
			flagBuilder.noLink(), 
			src.toString()
		).collect(joining(" "));
	}
	

	
	/**
	 * Use the specified {@code compilerProgram} as compiler program.
	 * 
	 * @param compilerProgram
	 * @return this
	 */
	public CompilerOperation<V> changeCompilerProgram(String compilerProgram) {
		this.compilerProgram = compilerProgram;
		return this;
	}
	
	/**
	 * Use the specified {@code flagsBuilder} as flags builder.
	 * 
	 * @param flagsBuilder flags builder
	 * @return this
	 */
	public CompilerOperation<V> changeFlagsBuilder(ICompilerFlagBuilder flagsBuilder) {
		this.flagBuilder = flagsBuilder;
		return this;
	}
	
	/**
	 * Use the specified {@code sourcesProvider} as sources provider.
	 * 
	 * @param sourcesProvider
	 * @return this
	 */
	public CompilerOperation<V> changeSourcesProvider(Function<V, Collection<Path>> sourcesProvider) {
		this.sourcesProvider = sourcesProvider;
		return this;
	}
	
	/**
	 * Use the specified {@code outputFileProvider}.
	 * 
	 * @param outputFileProvider
	 * @return this
	 */
	public CompilerOperation<V> changeOutputFileProvider(BiFunction<V, Path, Path> outputFileProvider) {
		this.outputFileProvider = outputFileProvider;
		return this;
	}
	
	/**
	 * Use the specified {@code includeDirsProvider}.
	 * 
	 * @param includeDirsProvider
	 * @return this
	 */
	public CompilerOperation<V> changeIncDirsProvider(BiFunction<V, Path, Collection<Path>> includeDirsProvider) {
		this.incDirsProvider =  includeDirsProvider;
		return this;
	}
	
	/**
	 * Use the specified {@code additionalFlagsProvider}.
	 * 
	 * @param additionalFlagsProvider
	 * @return this
	 */
	public CompilerOperation<V> changeFlagsProvider(BiFunction<V, Path, String> additionalFlagsProvider) {
		this.additionalFlagsProvider = additionalFlagsProvider;
		return this;
	}
	
}