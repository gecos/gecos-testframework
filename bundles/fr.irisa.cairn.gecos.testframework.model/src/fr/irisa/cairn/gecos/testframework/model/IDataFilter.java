package fr.irisa.cairn.gecos.testframework.model;

import java.util.function.Predicate;

@FunctionalInterface
public interface IDataFilter<T extends IData> extends Predicate<T> {

}
