package fr.irisa.cairn.gecos.testframework.exceptions;

public class CheckerFailure extends OperationFailureException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3331813459884797796L;

	/**
	 * Instantiates a new checker failure exception.
	 *
	 * @param msg the msg
	 */
	public CheckerFailure(String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new checker failure exception.
	 *
	 * @param string the string
	 * @param e the e
	 */
	public CheckerFailure(String string, Exception e) {
		super(string, e);
	}

}
