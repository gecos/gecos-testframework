package fr.irisa.cairn.gecos.testframework.dataprovider;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Used by {@link AbstractDataProvider} to append one String parameter 
 * to each provided data. The data is replicated for each of the
 * specified String values.
 * 
 * @author aelmouss
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Repeatable(AddStringParamsList.class)
public @interface AddStringParams {

	/**
	 * @return array of values for the string parameter to be added.
	 */
	String[] value();
	
}

