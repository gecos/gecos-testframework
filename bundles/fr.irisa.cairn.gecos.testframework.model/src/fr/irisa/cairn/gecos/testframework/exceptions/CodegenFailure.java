package fr.irisa.cairn.gecos.testframework.exceptions;

public class CodegenFailure extends OperationFailureException {

	/**
	 * Instantiates a new codegen failure exception.
	 *
	 * @param msg the msg
	 */
	public CodegenFailure(String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new codegen failure exception.
	 *
	 * @param string the string
	 * @param e the e
	 */
	public CodegenFailure(String string, Exception e) {
		super(string, e);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8740512042700764649L;

}
