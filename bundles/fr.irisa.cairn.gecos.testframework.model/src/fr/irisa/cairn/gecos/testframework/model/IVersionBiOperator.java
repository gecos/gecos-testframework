package fr.irisa.cairn.gecos.testframework.model;

import fr.irisa.cairn.gecos.testframework.exceptions.OperationFailureException;

/**
 * @author aelmouss
 */
@FunctionalInterface
public interface IVersionBiOperator<V extends IVersion> {

	/**
	 * Apply operation on the specified version pair.
	 * 
	 * @param v1 first version
	 * @param v2 second version
	 * @throws OperationFailureException in case comparison failed
	 */
	public void apply(V v1, V v2) throws OperationFailureException;

}
