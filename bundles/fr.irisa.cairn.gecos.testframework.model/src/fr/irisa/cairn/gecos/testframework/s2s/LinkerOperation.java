package fr.irisa.cairn.gecos.testframework.s2s;


import static fr.irisa.cairn.gecos.testframework.utils.OperationUtils.executeCmd;
import static fr.irisa.cairn.gecos.testframework.utils.OperationUtils.join;
import static java.util.stream.Collectors.joining;

import java.nio.file.Path;
import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Stream;

import fr.irisa.cairn.gecos.testframework.exceptions.LinkerFailure;
import fr.irisa.cairn.gecos.testframework.model.IVersion;
import fr.irisa.cairn.gecos.testframework.model.IVersionOperator;
import fr.irisa.cairn.gecos.testframework.s2s.CompilerOperation.ICompilerFlagBuilder;
import fr.irisa.cairn.gecos.testframework.utils.OperationUtils;

/**
 * This {@link IVersionOperator} implementation link code of a {@link IVersion}
 * by executing shell commands invoking a system linker (e.g. gcc, g++).
 * 
 * @author aelmouss
 */
public class LinkerOperation<V extends IVersion> implements IVersionOperator<V> {

	protected String linkerProgram;
	protected ICompilerFlagBuilder flagBuilder;
	protected Function<V, Collection<Path>> sourcesProvider;
	protected Function<V, Collection<Path>> libDirsProvider;
	protected Function<V, Collection<Path>> includeDirsProvider;
	protected Function<V, Collection<String>> libsProvider;
	protected Function<V, String> additionalFlagsProvider;
	
	public LinkerOperation(String linkerProgram, ICompilerFlagBuilder flagBuilder,
			Function<V, Collection<Path>> sourcesProvider,
			Function<V, Collection<Path>> includeDirsProvider,
			Function<V, Collection<Path>> libDirsProvider,
			Function<V, Collection<String>> libsProvider,
			Function<V, String> additionalFlagsProvider) {
		this.linkerProgram = linkerProgram;
		this.flagBuilder = flagBuilder;
		this.sourcesProvider = sourcesProvider;
		this.includeDirsProvider = includeDirsProvider;
		this.libDirsProvider = libDirsProvider;
		this.libsProvider = libsProvider;
		this.additionalFlagsProvider = additionalFlagsProvider;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Link the component of a version to produce the executable at the location 
	 * specified by {@link IVersion#getExecutable()}:
	 * 
	 * <p> The Linking command is constructed by {@link #getLinkCmd(IVersion)}, 
	 * then executed by {@link OperationUtils#executeCmd(String, Long)}.
	 *  
	 * @throws LinkerFailure if linking fails when stage is applied.
	 */
	@Override
	public void apply(V version) {
		try {
			String cmd = getLinkCmd(version);
			executeCmd(cmd, version.getTimeout());
		} catch(Exception e) {
			throw new LinkerFailure("Failed linking version '" + version + "'", e);
		}
	}
	
	/**
	 * Construct the command to link {@code version}.
	 * It uses:
	 * <li> {@link #linkerProgramProvider} to provide the 'LINKER' shell program
	 * <li> {@link #sourcesProvider} to provide link source (or object) files 'SRCS'
	 * <li> {@link #includeDirsProvider} to provide header files locations 'INC_DIRS'
	 * <li> {@link #linkerLibDirsProvider} to provide libraries locations 'LIB_DIRS'
	 * <li> {@link #linkerLibsProvider} to provide libraries 'LIBS'
	 * <li> {@link #additionalFlagsProvider} to provide any additional 'FLAGS' (NOT Libraries/Includes)
	 * 
	 * @param version the version to link
	 * @return "LINKER FLAGS -o version.getExecutable() version.getGeneratedObjects() SRCS INC_DIRS LIB_DIRS LIBS"
	 */
	protected String getLinkCmd(V version) {
		return Stream.of(
			linkerProgram,
			additionalFlagsProvider.apply(version),
			flagBuilder.outputFile(version.getExecutable().toString()),
			join(sourcesProvider.apply(version), Path::toString),
			join(includeDirsProvider.apply(version), d -> flagBuilder.includeDir(d.toString())),
			join(libDirsProvider.apply(version), d -> flagBuilder.libDir(d.toString())),
			join(libsProvider.apply(version)   , l -> flagBuilder.lib(l))
		).collect(joining(" "));
	}

	/**
	 * Use the specified {@code linkerProgram} as compiler program.
	 * 
	 * @param linkerProgram
	 * @return this
	 */
	public LinkerOperation<V> changeLinkerProgram(String linkerProgram) {
		this.linkerProgram = linkerProgram;
		return this;
	}
	
	/**
	 * Use the specified {@code flagsBuilder} as flags builder.
	 * 
	 * @param flagsBuilder flags builder
	 * @return this
	 */
	public LinkerOperation<V> changeFlagsBuilder(ICompilerFlagBuilder flagsBuilder) {
		this.flagBuilder = flagsBuilder;
		return this;
	}
	
	/**
	 * Use the specified {@code sourcesProvider} as sources provider.
	 * 
	 * @param sourcesProvider
	 * @return this.
	 */
	public LinkerOperation<V> changeSourcesProvider(Function<V, Collection<Path>> sourcesProvider) {
		this.sourcesProvider = sourcesProvider;
		return this;
	}
	
	/**
	 * Use the specified {@code includeDirsProvider}.
	 * 
	 * @param includeDirsProvider
	 * @return this.
	 */
	public LinkerOperation<V> changeIncDirsProvider(Function<V, Collection<Path>> includeDirsProvider) {
		this.includeDirsProvider = includeDirsProvider;
		return this;
	}
	
	/**
	 * Use the specified {@code libDirsProvider}.
	 * 
	 * @param libDirsProvider
	 * @return this.
	 */
	public LinkerOperation<V> changeLibDirsProvider(Function<V, Collection<Path>> libDirsProvider) {
		this.libDirsProvider = libDirsProvider;
		return this;
	}
	
	/**
	 * Use the specified {@code libsProvider}.
	 * 
	 * @param libsProvider
	 * @return this.
	 */
	public LinkerOperation<V> changeLibsProvider(Function<V, Collection<String>> libsProvider) {
		this.libsProvider = libsProvider;
		return this;
	}
	
	/**
	 * Use the specified {@code additionalFlagsProvider}.
	 * 
	 * @param additionalFlagsProvider
	 * @return this.
	 */
	public LinkerOperation<V> changeFlagsProvider(Function<V, String> additionalFlagsProvider) {
		this.additionalFlagsProvider = additionalFlagsProvider;
		return this;
	}
	
}