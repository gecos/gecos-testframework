package fr.irisa.cairn.gecos.testframework.utils;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;

public class ExceptionsHandler {

	/**
	 * Returns a {@link ExceptionsHandler} that does not match any
	 * exception.
	 *
	 * @return a new {@link ExceptionsHandler}
	 */
    public static ExceptionsHandler none() {
        return new ExceptionsHandler();
    }

    private final List<Matcher<? super Throwable>> exceptionMathchers = new ArrayList<>();

	private ExceptionsHandler() { }
	
    /**
     * Register the specified matcher.
     *
     * @param matcher a throwable matcher.
     */
    public void register(Matcher<? super Throwable> matcher) {
    	exceptionMathchers.add(matcher);
    }

	/**
	 * Return true if any registered throwable matcher matches {@code e}, false otherwise.
	 * 
	 * @param e a throwable to test
	 * @return true if {@code e} matches a registered throwable matcher, false otherwise.
	 */
    public boolean matches(Throwable e) {
		return !exceptionMathchers.isEmpty() && CoreMatchers.anyOf(exceptionMathchers).matches(e);
	}
    
    public boolean isAnyExceptionRegistered() {
        return !exceptionMathchers.isEmpty();
    }
    
    @Override
    public String toString() {
    	return exceptionMathchers.toString();
    }
}
