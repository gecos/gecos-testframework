package fr.irisa.cairn.gecos.testframework.utils;

import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.imageio.ImageIO;

public class GenInputFromImages {
	
	static private final String IMG_DIR = "./inputs/images/";
	static private final String OUTPUT_DIR = "./inputs/2d/";
	static private final String OUTPUT_EXTENSION = ".input";
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		
		File imgDir = new File(IMG_DIR);
		if(!imgDir.exists() || !imgDir.isDirectory()) {
			System.err.println("invalide image directory");
			return;
		}
		
		for(File imgFile : imgDir.listFiles()) {
			String imgName = imgFile.getName();
			
			try {
				BufferedImage img = ImageIO.read(imgFile);
				if(img.getType() != BufferedImage.TYPE_BYTE_GRAY) {
					System.err.println("Image is not byte grayscale! " + imgName);
					continue;
				}
				Raster data = img.getData();
				PrintWriter output = new PrintWriter(OUTPUT_DIR+imgName.substring(0,imgName.lastIndexOf("."))+OUTPUT_EXTENSION);
				output.println(data.getHeight());
				output.println(data.getWidth());
				for(int x=0; x<data.getHeight(); x++) {
					for(int y=0; y<data.getWidth(); y++) {
						int[] pixel= new int[1];
						data.getPixel(x, y, pixel);
						output.println(pixel[0]);
					}
				}
				output.close();
			} catch (IOException e) {
				System.err.println("Failed for: " + imgName);
			}
		}
	}
}
