package fr.irisa.cairn.gecos.testframework.s2s;

import static fr.irisa.cairn.gecos.testframework.stages.Stages.branch;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

import com.google.common.collect.LinkedHashMultimap;

import fr.irisa.cairn.gecos.testframework.exceptions.StopException;
import fr.irisa.cairn.gecos.testframework.model.ITestStage;
import fr.irisa.cairn.gecos.testframework.stages.Stages;


/**
 * Define a test flow used by {@link TestFlowTemplate}.
 * 
 * <p> The test flow has :<ul>
 * <li> <b>Named stages:</b> with names defined in {@link IStageName}.
 *   <p> They are meant represent the main steps of a source to source test flow.
 *   <p> These stages are considered in the same order defined by the ordinality 
 *   of the names.
 *   <p> They can be easily located an replaced using {@link #getStage(IStageName)}
 *   and {@link #replaceStage(IStageName, ITestStage)} ...
 *   <p> All named stages defaults to {@link Stages#NOP} if not specified.
 * 
 * <li> <b>Unnamed stages:</b> which are the stages that can be optionally added
 * after any named stage.
 *   <p> Such stages added after a named stage S, are considered in the same order 
 *   of their addition, after the named stage S.
 *   <p> The set of stages added after a given named stage S can be obtained/modified
 *   using {@link #getStagesAfter(IStageName)} ...
 * </ul>
 * 
 * <p> A builder pattern (using {@link #builder(Class, IStageName[])}) is provided to 
 * create an instance of this class. 
 * 
 * <p> Note that all operations performed by a stage are not run until
 * the test flow is applied.
 * 
 * @see TestFlowTemplate
 * @see IStageName
 * @see Stages
 * 
 * @author aelmouss
 */
public class TestFlow implements ITestStage {
	
	public static interface IStageName {
		public int ordinal(); 
	}
	
	protected IStageName[] names;
	protected LinkedHashMap<IStageName, ITestStage> namedStages;    // name1:stage1, name2:stage2, ...
	protected LinkedHashMultimap<IStageName, ITestStage> thenStages;// name1: [after stage 1 and before stage 2], ...
	
	protected TestFlow() {}
	
	/**
	 * Create a copy of this.
	 * 
	 * @return a copy of this flow
	 */
	public TestFlow copy() {
		TestFlow copy = new TestFlow();
		copy.namedStages = new LinkedHashMap<>(namedStages);
		copy.thenStages = LinkedHashMultimap.create(thenStages);
		return copy;
	}
	
	/**
	 * Replace the named stage by the specified one.
	 * 
	 * @param name the stage name
	 * @param newStage the stage value
	 * @return this
	 */
	public TestFlow replaceStage(IStageName name, ITestStage newStage) {
		namedStages.put(name, newStage);
		return this;
	}
	
	/**
	 * Replace the named stage by the specified one only if the predicate,
	 * applied to the test template, returns {@code true}. Otherwise do 
	 * nothing.
	 * 
	 * @param condition test the given test template.
	 * @param name the stage name
	 * @param newStage the stage value
	 * @return this
	 */
	public TestFlow replaceStageIf(IStageName name, ITestStage newStage, 
			Predicate<Object> condition) {
		replaceStage(name, branch(condition, newStage, getStage(name)));
		return this;
	}
	
	/**
	 * Replace the named stage by the one obtained by applying the specified
	 * {@code StorageManager} on it.
	 * 
	 * @param name the stage name
	 * @param stageModifier a function that return the new stage, given the old one.
	 * @return this
	 */
	public TestFlow modifyStage(IStageName name, Function<ITestStage, ITestStage> stageModifier) {
		replaceStage(name, stageModifier.apply(namedStages.get(name)));
		return this;
	}
	
	/**
	 * Insert the specified {@code stage} just after the named stage
	 * {@code name} i.e. on top of its then stages.
	 * 
	 * @param name the stage name
	 * @param stage to insert
	 * @return this
	 */
	public TestFlow addStageAfterFirst(IStageName name, ITestStage stage) {
		Set<ITestStage> set = thenStages.get(name);
		thenStages.removeAll(name);
		thenStages.putAll(name, set);
		return this;
	}
	
	/**
	 * Insert the specified {@code stage} at the end of the then stages
	 * (after) of the named stage {@code name}.
	 * 
	 * @param name the stage name
	 * @param stage to insert
	 * @return this
	 */
	public TestFlow addStageAfterLast(IStageName name, ITestStage stage) {
		thenStages.put(name, stage);
		return this;
	}
	
	/**
	 * @param name the stage name
	 * @return a named stage if found, {@link Stages#NOP} otherwise. 
	 */
	public ITestStage getStage(IStageName name) {
		ITestStage r = namedStages.get(name);
		return r == null ? Stages.NOP : r;
	}
	
	/**
	 * Return an ordered set of the unnamed stages that were added after the 
	 * named stage, identified by {@code name}, until the next initialized named stage.
	 * 
	 * <p> Changes to the returned set will update the underlying 
	 * multimap, and vice versa.
	 * 
	 * @param name the stage name
	 * @return {@code null} if none found.
	 */
	public Set<ITestStage> getStagesAfter(IStageName name) {
		return thenStages.get(name);
	}
	
	/**
	 * Return an ordered set of unnamed stages added after the previous 
	 * initialized named stage, identified by {@code name}.
	 * 
	 * <p> Changes to the returned set will update the underlying 
	 * multimap, and vice versa.
	 * 
	 * @param name the stage name
	 * @return {@code null} if none found.
	 */
	public Set<ITestStage> getStagesBefore(IStageName name) {
		int previousNamed = name.ordinal() - 1;
		while(previousNamed >= 0) {
			Set<ITestStage> r = thenStages.get(names[previousNamed]);
			if(r == null || r.isEmpty())
				previousNamed--;
		}
		
		if(previousNamed >= 0)
			return thenStages.get(names[previousNamed]);
		return null;
	}

	
	@Override
	public void apply(Object target) {
		List<ITestStage> stages = buildFlow();
		
		if(!stages.isEmpty())
			Stages.chain(stages).apply(target);
		else
			throw new StopException("The test flow is empty or has only has NOP stages!");
	}

	private List<ITestStage> buildFlow() {
		List<ITestStage> stages = new ArrayList<>();
		for(IStageName name : names) {
			ITestStage n = namedStages.get(name);
			if(n != null && n != Stages.NOP)
				stages.add(n);
			
			Set<ITestStage> u = thenStages.get(name);
			if(u != null) {
				u.stream()
					.filter(s -> s != null && s != Stages.NOP)
					.forEach(stages::add);
			}
		}
		return stages;
	}
	
	/**
	 * Create a {@link TestFlow} builder using the specified names.
	 * <p> The ordinality of names dictates the order of application of the 
	 * corresponding named stages in the flow being built. First named 
	 * stage is applied first..
	 * 
	 * @param names can be constructed as the values of an emun implementing
	 * {@link IStageName}.
	 * 
	 * @return a new builder
	 */
	public static TestFlowBuilder builder( IStageName[] names) {
		return new TestFlowBuilder(names);
	}
	
}