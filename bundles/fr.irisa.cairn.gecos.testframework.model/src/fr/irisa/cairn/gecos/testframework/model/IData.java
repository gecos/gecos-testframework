package fr.irisa.cairn.gecos.testframework.model;

/**
 * @author aelmouss
 */
public interface IData {

	/**
	 * Get a name for this data.
	 *
	 * @return the data name
	 */
	public String getName();

}
