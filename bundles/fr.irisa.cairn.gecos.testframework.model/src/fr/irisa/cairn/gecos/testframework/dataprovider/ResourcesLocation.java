package fr.irisa.cairn.gecos.testframework.dataprovider;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.nio.file.Path;

import fr.irisa.cairn.gecos.testframework.data.AbstractDataFromPath;
import fr.irisa.cairn.gecos.testframework.data.CxxFileData;
import fr.irisa.cairn.gecos.testframework.data.CxxFilesGroupData;
import fr.irisa.cairn.gecos.testframework.data.CxxMakefileData;

/**
 * This annotation is used by {@link DataFromPathProvider} to specify
 * the resources location(s).
 * 
 * <p> A location is resolved as a path, specified by {@link #value()},
 * relative to a bundle, specified by {@link #bundleName()},
 * 
 * <p> The location is searched recursively by {@link DataFromPathProvider},
 * for {@link Path}s compatible with at least one of the {@link AbstractDataFromPath}
 * classes specified by {@link #dataClasses()}.
 * 
 * <p> The number of data provided from this location, can be limited
 * using {@link #limit()}. 
 * 
 * @author aelmouss
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Repeatable(ResourcesLocationList.class)
public @interface ResourcesLocation {
	
	/**
	 * Specifies the name of the bundle that contains the resources directory
	 * specified by {@link #value()}.
	 * 
	 * <p> If null or empty, the bundle that contains the declaring class of 
	 * the {@link Method} annotated by this, will be used by {@link DataFromPathProvider}.
	 * 
	 * <p> Defaults to an empty String (i.e. use "current" bundle).
	 * 
	 * @return the name of the bundle that contains the resources directory.
	 */
	String bundleName() default "";
	
	/**
	 * Specifies the path of the resources directory relative to the 
	 * location of the bundle specified by {@link #bundleName()}.
	 * 
	 * @return path of the resources directory relative to bundle.
	 */
	String value();
	
	/**
	 * Array of {@link AbstractDataFromPath}s classes that define what data to look for.
	 * 
	 * <p> The classes are checked in order, so for a given resource path, the first compatible
	 * data class is used to create the corresponding data instance.
	 * 
	 * <p><b>NOTE:</b> 
	 * all data classes MUST satisfy all criteria imposed by {@link AbstractDataFromPath}!
	 * 
	 * <p> Defaults to [{@link CxxFilesGroupData}.class, {@link CxxMakefileData}.class, 
	 *  {@link CxxFileData}.class]
	 */
	Class<? extends AbstractDataFromPath>[] dataClasses() default {
		CxxFilesGroupData.class, CxxMakefileData.class, CxxFileData.class
	} ;
	
	/**
	 * Instructs {@link DataFromPathProvider} to limit the number of data
	 * it provides to the specified limit. No limit is applied if negative.
	 * 
	 * <p> Defaults to -1 (i.e. no limit).
	 * 
	 * @return the maximum number of data {@link DataFromPathProvider} will
	 * provide from this location
	 */
	int limit() default -1; 
}
