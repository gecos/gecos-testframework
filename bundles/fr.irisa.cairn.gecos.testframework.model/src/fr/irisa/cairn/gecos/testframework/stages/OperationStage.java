//package fr.irisa.cairn.gecos.testframework.stages;
//
//import java.util.Objects;
//import java.util.function.Predicate;
//import java.util.stream.Stream;
//
//import fr.irisa.cairn.gecos.testframework.model.AbstractTestTemplate;
//import fr.irisa.cairn.gecos.testframework.model.ITestStage;
//import fr.irisa.cairn.gecos.testframework.model.IVersion;
//import fr.irisa.cairn.gecos.testframework.model.IVersionOperator;
//
///**
// * Register operations to apply on each version in the
// * {@link #apply(Object) apply} target.
// * 
// * <p> Versions are processed sequentially, unless this Stage is made
// * parallel by invoking {@link #makeParallel()}. In this case, the data
// * stream is made parallel first.
// * 
// * @author aelmouss
// */
//public class OperationStage<V extends IVersion> implements ITestStage {
//	
//	protected Predicate<V> condition;
//	protected IVersionOperator<V> trueOperator;
//	protected IVersionOperator<V> falseOperator;
//	protected boolean parallel = false;
//
//	/**
//	 * When this stage is run, apply {@code ifTrueOperator} on each version for which
//	 * {@code condition} evaluates to {@code true}.
//	 * 
//	 * <p> Otherwise, if {@code ifFalseOperator} is not {@code null}, apply it on the 
//	 * remaining versions (i.e. for which {@code condition} evaluates to {@code false}.
//	 * 
//	 * <p> A {@code null} {@code condition} is equivalent to {@code v -> true} i.e.
//	 * {@code ifTrueOperator} is applied on all versions.
//	 * 
//	 * @param condition if {@code null}, @code ifTrueOperator} is applied on all versions.
//	 * @param ifTrueOperator to apply of versions passing condition. Cannot be {@code null}.
//	 * @param ifFalseOperator to apply of versions not passing condition. If {@code null}
//	 * no operation is applied on such versions.
//	 */
//	public OperationStage(Predicate<V> condition, IVersionOperator<V> ifTrueOperator, 
//			IVersionOperator<V> ifFalseOperator) {
//		Objects.requireNonNull(ifTrueOperator);
//		this.condition = condition; 
//		this.trueOperator = ifTrueOperator;
//		this.falseOperator = ifFalseOperator;
//	}
//
//	/**
//	 * not tested !
//	 */
//	public OperationStage<V> makeParallel() {
//		this.parallel = true;
//		return this;
//	}
//
//	@Override
//	public void apply(Object target) {
//		@SuppressWarnings("unchecked")
//		AbstractTestTemplate<V> testTemplate = (AbstractTestTemplate<V>)target;
//		
//		Stream<V> versions = testTemplate.getVersions().stream();
//		if(parallel)
//			versions = versions.parallel();
//		
//		versions.forEach(v -> {
//			IVersionOperator<V> operator = condition == null ? trueOperator :
//					condition.test(v) ? trueOperator : falseOperator;
//			 
//			operator.apply(v);
//		});
//	}
//	
//}